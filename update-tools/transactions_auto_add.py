from piecash import open_book, Transaction, Split, Account, GncImbalanceError, factories
from piecash.core.session import build_uri

from datetime import datetime
from decimal import Decimal

uri = build_uri(db_type="mysql", db_user="udai", db_password="1337MoTo", db_name="UdaiGNUCash", 
                       db_host="finance.udaikiran.com", db_port=3306)

with open_book(uri_conn=uri, open_if_lock=True, readonly=False, do_backup=False) as book:
    today = datetime.today()
    INR = book.default_currency
    amount = 30

    print(INR)
    to_account = book.accounts(fullname="Expenses:Clothes")
    from_account = book.accounts(fullname="Assets:Current Assets:Cash in Wallet")

    print(to_account.name, from_account.name)
    Transaction(
        currency = INR,
        description = "Test Transaction",
        splits=[
            Split(account = from_account, value = -1*amount, memo = "Split memo 1"),
            Split(account = to_account, value = amount, memo = "another split")
        ]
    )
    book.save()

