import sys
from selenium import webdriver 
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup
from crypto import AESCipher
import getpass
import pickle

enc_sbi_username = b'NZDJiNV6/Us9uGd/9HiyDwb7WjJZfQhcgrO1K72dVxTUP4Gtci6ufOiBwJdme4jZ'
enc_sbi_password = b'SepuC4CWrDb0WhLGKdurvJu8HBPawgqONAh5lzB8sPjZaC6LIGx4P6vmwC9vvJja'

try:
    dec_password = getpass.getpass()
except Exception as err:
   print('ERROR:', err)
else:
    print('Input password done')

cipher = AESCipher(dec_password)

username = cipher.decrypt(enc_sbi_username)
password = cipher.decrypt(enc_sbi_password)

option = webdriver.ChromeOptions()
option.add_argument(" — incognito")

browser = webdriver.Chrome(executable_path='/sware/chromedriver/chromedriver', chrome_options=option)
browser.get("https://retail.onlinesbi.com/retail/login.htm")
browser.find_element_by_css_selector('.login_button').click()

user_field = browser.find_element_by_name('userName')
user_field.send_keys(username)

password_field = browser.find_element_by_name('password')
password_field.send_keys(password)

username = None
password = None
dec_password = None

browser.find_element_by_id('Button2').click()
browser.find_element_by_link_text('Account Statement').click()
browser.find_element_by_id('drd1').click()
browser.find_element_by_id('bymonth').click()
browser.find_element_by_id('Submit3').click()

html_source = browser.page_source
soup = BeautifulSoup(html_source, 'html.parser')
browser.find_element_by_link_text("LOGOUT").click()
browser.close()

data = []
table = soup.find('table', attrs={'class':'table table-hover table-bordered content_table table-striped'})
table_body = table.find('tbody')

rows = table_body.find_all('tr')
for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    data.append([ele for ele in cols])

pickle.dump(data, open( "data1.p", "wb" ) )
