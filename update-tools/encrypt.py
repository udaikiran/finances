from crypto import AESCipher
import getpass

try:
    dec_password = getpass.getpass()
except Exception as err:
   print('ERROR:', err)
else:
    print('Input password done')

cipher = AESCipher(dec_password)

username = input("username:")
password = getpass.getpass()

username = cipher.encrypt(username)
password = cipher.encrypt(password)

print(username, password)