-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: UdaiGNUCash
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

USE UdaiGNUCash;

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `guid` varchar(32) NOT NULL,
  `name` varchar(2048) NOT NULL,
  `account_type` varchar(2048) NOT NULL,
  `commodity_guid` varchar(32) DEFAULT NULL,
  `commodity_scu` int(11) NOT NULL,
  `non_std_scu` int(11) NOT NULL,
  `parent_guid` varchar(32) DEFAULT NULL,
  `code` varchar(2048) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `hidden` int(11) DEFAULT NULL,
  `placeholder` int(11) DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES ('03f307b3bfc29b4b9d2cab074274ebf0','Fees','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'ffe550729c1a98aa720c4b528afbdc8e','','Fees',0,0),('0731bc05804a4e132fbfa611a4ea1138','Tithe-Lords Church','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'0863b4cc996155d0335bae2a743ee9c1','Tithe-Lords Church','Tithe-Lords Church',0,0),('0863b4cc996155d0335bae2a743ee9c1','Gifts','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','Gifts','Gifts',0,1),('0b50a16343d5e07190dfa91d0fe91355','Supreme Industries Ltd','STOCK','335ea05a3d3594e7253f10c81ba397bb',1,0,'13e00c9b7ba95740fcebf1285328bf1a','SUPREMEIND','Supreme Industries Ltd',0,0),('0d21fec3e0fcb1619dc17f6fe139efc8','Joint SBI Account','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'2c1729025ff9cb85c4ee772511682ed6','00000020095397245','SBCHQ-GEN-PUB-IND-PBB-INR',0,0),('0d3b4bbd48451db7ce5b25b17aa2feff','Insurance','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Insurance',0,0),('0e31f81088bbdcba65ea21fe1900aa25','SGST','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'795384b4b3b6340605340b15b6e6a038','SGST','SGST',0,0),('107f66386bd5dc23397ca95df4253044','CGST','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'795384b4b3b6340605340b15b6e6a038','CGST','CGST',0,0),('127a7bccdea13690d45abef992b2779f','Everest Industries Limited','STOCK','d413dfa6bef977f2cf0b7510f8e2a97c',1,0,'13e00c9b7ba95740fcebf1285328bf1a','EVERESTIND','Everest Industries Limited',0,0),('1314f73315a76aec740d2d3fc97bbd29','Parking','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'ffe550729c1a98aa720c4b528afbdc8e','','Parking',0,0),('13e00c9b7ba95740fcebf1285328bf1a','Stocks','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'15bcc94e5ed56590e961031aa6f4922b','','Stock',0,1),('144657e137658a89cde4b307d457bfa5','Savings Interest','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cf0f5ad07f5aff7db4b56bcb737e25a1','','Savings Interest',0,0),('15bcc94e5ed56590e961031aa6f4922b','Brokerage Account','BANK','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'54dfcc64b77c1d1c11449174f1716912','','Brokerage Account',0,0),('193f6f2a73ce82c9cc453dd599273b6c','HDFC Bank Limited','STOCK','4afbe733c6ccea5f63fa4d4b12bb4b62',1,0,'13e00c9b7ba95740fcebf1285328bf1a','HDFCBANK','HDFC Bank Limited',0,0),('1bc887b0cbfcae70284b8e099164bd22','Equity','EQUITY','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1bfc6447ffbc1d74a3b8266918768988','','Equity',0,1),('1bfc6447ffbc1d74a3b8266918768988','Root Account','ROOT','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,NULL,'','',0,0),('1e308c9617e0f63056bb6ac91f3308f7','Water','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'dd7e2d259545e057bf128fa3976a653b','','Water',0,0),('1e46b42113fdd7774bef907f11c59bd7','Travel','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Travel',0,0),('1edf8a7638e59599acbbe57ac24f7a8f','ICICI Pru Value Discovery Direct-G','MUTUAL','9f057231ab9513897ca01f565581ca57',10000,0,'29e39e797f1f0d7c06b712f5a153798d','ICICI Pru Value Discovery Direct-G','ICICI Pru Value Discovery Direct-G',0,0),('203ea0bbbf9cae3fc40bd8aa81e43caf','EPFO - Employee Contribution','BANK','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'a1164e62e7bd81d0e7c52701847f3e8f','','EPFO - Employee Contribution',0,0),('27c4224f38bff085ef3bf0d61833e522','ICICI Bank Account','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'2c1729025ff9cb85c4ee772511682ed6','131601001257','ICICI Bank Account',0,0),('29e39e797f1f0d7c06b712f5a153798d','Mutual Fund','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'15bcc94e5ed56590e961031aa6f4922b','','Mutual Fund',0,0),('2c1729025ff9cb85c4ee772511682ed6','Bank Accounts','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'2e05ce283fee5ae9134493d71849e4e2','Bank Accounts','Top level for all accounts',0,1),('2da96fc6d7160ed1faa3530e011ac6ae','EPFO - Pension Contribution','BANK','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'a1164e62e7bd81d0e7c52701847f3e8f','','EPFO - Pension Contribution',0,0),('2e05ce283fee5ae9134493d71849e4e2','Assets','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1bfc6447ffbc1d74a3b8266918768988','','Assets',0,1),('2fae6d0f0f15f42e208beedc9f2f6614','Repair and Maintenance','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'ffe550729c1a98aa720c4b528afbdc8e','','Repair and Maintenance',0,0),('3c237ec529e2c2c313ca60e2129dabcc','Groceries','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','Groceries','Groceries',0,0),('3d87a9a6fa3c92191e146c44115f0c07','HDFC Bank Account','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'2c1729025ff9cb85c4ee772511682ed6','10311000013510','HDFC Bank Savings Account',0,0),('3f3a7895f1bcd173de594539b1e12c48','SBI Card','CREDIT','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'63ab6c713fe80a46064ad97783fddbcd','XXXX-XXXX-XXXX-9690','SBI Credit Card',0,0),('3fc86d569a9e7a27767ac24e7ec1aa18','Maintenance','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'4b3e074809e45e9bc6e8d2184b6f10d3','Maintenance','Maintenance',0,0),('407dba4b7009670c78d7fb3aa3dfa851','Bank Service Charge','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Bank Service Charge',0,0),('422a30a247933efb47b17e9638722da7','Snacks','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'72fb0f4ab210fc19df29895461406374','Snacks','Snacks',0,0),('446050a5d3b0efa8a78803a133808b8c','EPFO - Employer Contribution','BANK','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'a1164e62e7bd81d0e7c52701847f3e8f','100402142362','EPFO - Employer Contribution',0,0),('455af3d10bb5a2c5b9c905afa64bfe5d','Monthly Household Expeneses','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'7a58f1efc9a4125a2c483cd1eeccfb53','','Monthly Household Expeneses',0,0),('46fbf77c4e9831aa218b6611654c4c73','HDFC Bank','CREDIT','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'63ab6c713fe80a46064ad97783fddbcd','545964XXXXXX7462','HDFC Bank CC',0,0),('4860fbd1bf2ac1587680e5b36bd7e436','Cash in Wallet','CASH','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'e0b8b5011fa1489874e9f3033b6a4806','','Cash in Wallet',0,0),('49cf17633d5acb5452d239caa8009cb9','Liabilities','LIABILITY','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1bfc6447ffbc1d74a3b8266918768988','','Liabilities',0,1),('4b3e074809e45e9bc6e8d2184b6f10d3','Home Expenses','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','Home Expenses','Home Expenses',0,1),('4ce88194ae2bf4414fbaa5f7972a724e','PPF Account','BANK','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'c880925e86f2d464085bd468e04d3b1b','00000032491610655','PPF Account',0,0),('5242f3b0472b95d88ffc7c548b11e3d6','Salary Account SBI','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'2c1729025ff9cb85c4ee772511682ed6','00000020077230265','SBCHQ-CSA-PUB-IND-CSPLT-INR',0,0),('54dfcc64b77c1d1c11449174f1716912','Investments','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'2e05ce283fee5ae9134493d71849e4e2','','Investments',0,1),('58128fb41bfd11077b336535fdb2f407','Ujjivan Financial Services Ltd','STOCK','e21e7533eb970899b50177c0c50ce0b2',1,0,'13e00c9b7ba95740fcebf1285328bf1a','UJJIVAN','Ujjivan Financial Services Ltd',0,0),('5928d89aa64b5a59e24e164a5ef04daf','Interest','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Interest',0,0),('5986ada838a621cd06fae5c027fed6c7','Stationary','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Stationary',0,0),('60a0305b5804a0cba1be5fe37cd9a3ec','Bond Interest','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cf0f5ad07f5aff7db4b56bcb737e25a1','','Bond Interest',0,0),('63ab6c713fe80a46064ad97783fddbcd','Credit Cards','CREDIT','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'49cf17633d5acb5452d239caa8009cb9','','Credit Cards',0,1),('64488401e841de1e52a1393fb6e53746','Bond','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'15bcc94e5ed56590e961031aa6f4922b','','Bond',0,0),('649848eaf518be30a3f00ee5453bb59b','Opening Balances','EQUITY','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1bc887b0cbfcae70284b8e099164bd22','','Opening Balances',0,0),('655a5f351e88e3cf86662051d3968043','Template Root','ROOT',NULL,0,0,NULL,'','',0,0),('6965f0a52e99ed48269f9b7f60797f42','UberCab','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1e46b42113fdd7774bef907f11c59bd7','UberCab','UberCab',0,0),('6ddda8e5c5fe1717377da46c70b2d66a','Mirae Asset Emerging Bluechip Direct-G','MUTUAL','dd2ee5010f2d5c800af4a93e85c39a81',10000,0,'29e39e797f1f0d7c06b712f5a153798d','Mirae Asset Emerging Bluechip Direct-G','Mirae Asset Emerging Bluechip Direct-G',0,0),('7216687f8a712f14af685716d2eead70','Petrol','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'ffe550729c1a98aa720c4b528afbdc8e','','Petrol',0,0),('7253248269cfd24da0a6c3ac94cf4d8a','Franklin India Smaller Companies Direct-G','MUTUAL','bc5b6e653b7fa594bb97c8fefe42b244',10000,0,'29e39e797f1f0d7c06b712f5a153798d','Franklin India Smaller Companies Direct-G','Franklin India Smaller Companies Direct-G',0,0),('72fb0f4ab210fc19df29895461406374','Dining','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','Dining','Dining',0,0),('73e0d82638c6ab8a656009e2ecaa8dad','Ammamma-Gift','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'0863b4cc996155d0335bae2a743ee9c1','Ammamma-Gift','Ammamma-Gift',0,0),('795384b4b3b6340605340b15b6e6a038','Taxes','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Taxes',0,0),('7a58f1efc9a4125a2c483cd1eeccfb53','Maintenance','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','Maintenance','Maintenance',0,1),('7f1aec77ac35b0e3c92a22cac0db86e5','Tithe-IPHC Hyderabad','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'0863b4cc996155d0335bae2a743ee9c1','Tithe-IPHC Hyderabad','Tithe-IPHC Hyderabad',0,0),('884670abc0e614e6c2ba581ec33926d9','Fruits','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'3c237ec529e2c2c313ca60e2129dabcc','Fruits','Fruits',0,0),('88716ba0cae509ed5d3ace0b87e81522','Gifts Received','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cc4f98a8aeedef3908a5075f0b2840a5','','Gifts Received',0,0),('88ec26a19ae851cfbaafcd1b42f6ee42','Phone','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'dd7e2d259545e057bf128fa3976a653b','Phone','Phone',0,0),('8fc91fbe3db81364bd56d3d4a4853807','Citibank','CREDIT','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'63ab6c713fe80a46064ad97783fddbcd','4386XXX0XXXX1233','Citi Bank CC',0,0),('9165b002d366de56b25ce0f6c96475a2','Dividend Income','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cc4f98a8aeedef3908a5075f0b2840a5','','Dividend Income',0,0),('925544064b77d21245b385e13c252d84','LIC Housing Finance Limited','STOCK','d60159942c6afeca3dec68b68dbd0593',1,0,'13e00c9b7ba95740fcebf1285328bf1a','LICHSGFIN','LIC Housing Finance Limited',0,0),('9271ca1a07bf91fa11a6ec02d984939d','SBI Mortgage Loan','LIABILITY','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'9fc2fff884cb47b1b9c14aac3d73ba85','00000034497003740','MC-SBI HL MAXGAIN (WOM) JUN 17',0,0),('946bf0729b8cdf2db077b8b4df60fa3e','GRUH Finance Ltd.','STOCK','7c97e2677a6ddae83a78313fd3252f5c',1,0,'13e00c9b7ba95740fcebf1285328bf1a','GRUH','GRUH Finance Ltd.',0,0),('95b215856f596dab5f28f3e5d6eab750','Other Interest','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'5928d89aa64b5a59e24e164a5ef04daf','','Other Interest',0,0),('97f7a4111779ba6d2e85291efbd670ea','Garbage collection','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'dd7e2d259545e057bf128fa3976a653b','','Garbage collection',0,0),('98b87123589a93713b98c61d5276baa5','Groceries','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'3c237ec529e2c2c313ca60e2129dabcc','Groceries','Groceries',0,0),('9985a704f5836e1c364237bd30f7de11','Income Tax','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'795384b4b3b6340605340b15b6e6a038','Income Tax','Central Income Tax',0,0),('9dadd41c01c1f2f4b8a3bab176a122e4','Market Index','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'15bcc94e5ed56590e961031aa6f4922b','','Market Index',0,0),('9db15a2d6747ef2d7c09708f8af91d97','Other','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1e46b42113fdd7774bef907f11c59bd7','Other','Other',0,0),('9fc2fff884cb47b1b9c14aac3d73ba85','Loans','LIABILITY','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'49cf17633d5acb5452d239caa8009cb9','','Loans',0,1),('a1164e62e7bd81d0e7c52701847f3e8f','EPFO ','BANK','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'c880925e86f2d464085bd468e04d3b1b','EPFO','EPFO',0,1),('a16a0747f1dc0fa26dc3223ac820c2e5','Adjustment','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Adjustment',0,0),('a186c08bd2261105c3caccbd1538620d','Imbalance-INR','BANK','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1bfc6447ffbc1d74a3b8266918768988','','',0,0),('a9e9ffb50d365ee4990c529fc6e81220','Misc','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','Misc','Misc',0,0),('aac3cb442103091201a19a195ba28ad0','Life Insurance','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'0d3b4bbd48451db7ce5b25b17aa2feff','','Life Insurance',0,0),('ae8891bb416c6908b1e5c1e9c2c42660','Intermediary Account','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'e0b8b5011fa1489874e9f3033b6a4806','Intermediary Account','Intermediary Account',0,0),('aebb22fc50169e1a556deaa898a3337c','Mutual Funds','MUTUAL','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'c880925e86f2d464085bd468e04d3b1b','','Mutual Funds',0,1),('b3b5229aec106ae3df576ab19e316096','Checking Interest','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cf0f5ad07f5aff7db4b56bcb737e25a1','','Checking Interest',0,0),('b5add894133fa37d07ea514ecd8ccf7e','Train-IRCTC','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1e46b42113fdd7774bef907f11c59bd7','Train-IRCTC','Train-IRCTC',0,0),('b8e7452aafccd6e4d0db1484c0d55804','Expenses','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1bfc6447ffbc1d74a3b8266918768988','','Expenses',0,1),('c2ac4f942239a5afafd5edec7fcfe422','Gas','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'dd7e2d259545e057bf128fa3976a653b','','Gas',0,0),('c36b3ebdd1e1701486319297cc13fe64','Paytm','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'e0b8b5011fa1489874e9f3033b6a4806','9949989024','Paytm',0,0),('c46156546b88ad5bb27f2d3dba01aebb','Clothes','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Clothes',0,0),('c880925e86f2d464085bd468e04d3b1b','Retirement','BANK','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'54dfcc64b77c1d1c11449174f1716912','','Retirement',0,1),('caf3feba0afc6f0b9f7bce678b56f392','Restaurant Dining','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'72fb0f4ab210fc19df29895461406374','Restaurant Dining','Restaurant Dining',0,0),('cb2d2284961367f8f95df33f9e2cde6b','Food Coupons','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'e0b8b5011fa1489874e9f3033b6a4806','Food Coupons','Food Coupons',0,0),('cb41053e15d88ee4c22d0320b84e9519','Electricity','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'dd7e2d259545e057bf128fa3976a653b','','Electricity',0,0),('cc4f98a8aeedef3908a5075f0b2840a5','Income','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1bfc6447ffbc1d74a3b8266918768988','','Income',0,1),('cf0f5ad07f5aff7db4b56bcb737e25a1','Interest Income','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cc4f98a8aeedef3908a5075f0b2840a5','','Interest Income',0,0),('d02725812903f4528424e2a719818393','Subscriptions','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Subscriptions',0,0),('dd7e2d259545e057bf128fa3976a653b','Utilities','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Utilities',0,0),('e0b8b5011fa1489874e9f3033b6a4806','Current Assets','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'2e05ce283fee5ae9134493d71849e4e2','','Current Assets',0,1),('e25012118b8a86970898b0fe2fcea20a','OlaCab','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'1e46b42113fdd7774bef907f11c59bd7','OlaCab','OlaCab',0,0),('e51c1f47b657d70f26bf160e30b35354','Brokerage Margin','BANK','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'15bcc94e5ed56590e961031aa6f4922b','Brokerage Margin','Brokerage Margin',0,0),('e692e6912549941cecf2fcf11ab30ba1','Health Insurance','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'0d3b4bbd48451db7ce5b25b17aa2feff','','Health Insurance',0,0),('e7db8aaedf902655b44dc027ee03bb60','Aditya Birla SL Small & Midcap Direct-G','MUTUAL','854c51f2837b3c49b130a5792d1dbc11',10000,0,'29e39e797f1f0d7c06b712f5a153798d','Aditya Birla SL Small & Midcap Direct-G','Aditya Birla SL Small & Midcap Direct-G',0,0),('eba66f9e03bcfe6d853da967f6ec18ea','TeamF1 Networks Salary','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cc4f98a8aeedef3908a5075f0b2840a5','TeamF1 Networks Salary','TeamF1 Networks Salary',0,0),('eceb6509415a2c2ffab48041ca9d7edb','DSPBR Midcap Direct-G','MUTUAL','447cde22fd642a46019ba30084fc4dfc',10000,0,'29e39e797f1f0d7c06b712f5a153798d','DSPBR Midcap Direct-G','DSPBR Midcap Direct-G',0,0),('ee3a5de26dcbed105dbcbc629fd54612','SBI-HomeLoan-Investment','ASSET','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'2c1729025ff9cb85c4ee772511682ed6','SBI-HomeLoan-Investment','SBI-HomeLoan-Investment',0,0),('eee1570cb845f0f957c7ee5302905367','Cashback','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cc4f98a8aeedef3908a5075f0b2840a5','Cashback','Cashback',0,0),('f2733666808f64e1da7434dfacf45a39','Other Income','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cc4f98a8aeedef3908a5075f0b2840a5','','Other Income',0,0),('f36e7e8b06eb3e567186b1b7148208e5','Professional Tax','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'795384b4b3b6340605340b15b6e6a038','Professional Tax','Professional Tax',0,0),('f545e2919b030e17bf7e29c0b11f031c','Bonus','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cc4f98a8aeedef3908a5075f0b2840a5','','Bonus',0,0),('f7c91d4ec504bf4ea99402e05ff872fa','Other Interest','INCOME','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'cf0f5ad07f5aff7db4b56bcb737e25a1','','Other Interest',0,0),('f97a1cc92bbd40fd14d13b44490cc770','Franklin India High Growth Companies Direct-G','MUTUAL','019fc6a229547340923551eea7019f6d',10000,0,'29e39e797f1f0d7c06b712f5a153798d','Franklin India High Growth Companies Direct-G','Franklin India High Growth Companies Direct-G',0,0),('fe12f249166b5f710c141512437f1aa4','Auto Insurance','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'0d3b4bbd48451db7ce5b25b17aa2feff','','Auto Insurance',0,0),('fe1e10f543b3b176e0ab0ba6e4455ea4','Mortgage Interest','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'5928d89aa64b5a59e24e164a5ef04daf','','Mortgage Interest',0,0),('fe879ccb6fe227e284eb043f6a3998ef','House Tax','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'795384b4b3b6340605340b15b6e6a038','House Tax','House Tax',0,0),('ffe550729c1a98aa720c4b528afbdc8e','Bike','EXPENSE','b417a83b048dbde8ef39c41ba0f0b0ce',100,0,'b8e7452aafccd6e4d0db1484c0d55804','','Bike',0,0);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `billterms`
--

DROP TABLE IF EXISTS `billterms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billterms` (
  `guid` varchar(32) NOT NULL,
  `name` varchar(2048) NOT NULL,
  `description` varchar(2048) NOT NULL,
  `refcount` int(11) NOT NULL,
  `invisible` int(11) NOT NULL,
  `parent` varchar(32) DEFAULT NULL,
  `type` varchar(2048) NOT NULL,
  `duedays` int(11) DEFAULT NULL,
  `discountdays` int(11) DEFAULT NULL,
  `discount_num` bigint(20) DEFAULT NULL,
  `discount_denom` bigint(20) DEFAULT NULL,
  `cutoff` int(11) DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billterms`
--

LOCK TABLES `billterms` WRITE;
/*!40000 ALTER TABLE `billterms` DISABLE KEYS */;
/*!40000 ALTER TABLE `billterms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `guid` varchar(32) NOT NULL,
  `root_account_guid` varchar(32) NOT NULL,
  `root_template_guid` varchar(32) NOT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES ('7505cd9c8cb2fc8127777312c8198d9d','1bfc6447ffbc1d74a3b8266918768988','655a5f351e88e3cf86662051d3968043');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `budget_amounts`
--

DROP TABLE IF EXISTS `budget_amounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget_amounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `budget_guid` varchar(32) NOT NULL,
  `account_guid` varchar(32) NOT NULL,
  `period_num` int(11) NOT NULL,
  `amount_num` bigint(20) NOT NULL,
  `amount_denom` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget_amounts`
--

LOCK TABLES `budget_amounts` WRITE;
/*!40000 ALTER TABLE `budget_amounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget_amounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `budgets`
--

DROP TABLE IF EXISTS `budgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budgets` (
  `guid` varchar(32) NOT NULL,
  `name` varchar(2048) NOT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `num_periods` int(11) NOT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budgets`
--

LOCK TABLES `budgets` WRITE;
/*!40000 ALTER TABLE `budgets` DISABLE KEYS */;
/*!40000 ALTER TABLE `budgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commodities`
--

DROP TABLE IF EXISTS `commodities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commodities` (
  `guid` varchar(32) NOT NULL,
  `namespace` varchar(2048) NOT NULL,
  `mnemonic` varchar(2048) NOT NULL,
  `fullname` varchar(2048) DEFAULT NULL,
  `cusip` varchar(2048) DEFAULT NULL,
  `fraction` int(11) NOT NULL,
  `quote_flag` int(11) NOT NULL,
  `quote_source` varchar(2048) DEFAULT NULL,
  `quote_tz` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commodities`
--

LOCK TABLES `commodities` WRITE;
/*!40000 ALTER TABLE `commodities` DISABLE KEYS */;
INSERT INTO `commodities` VALUES ('019fc6a229547340923551eea7019f6d','FUND','118564','Franklin India High Growth Companies Fund - Direct Plan','INF090I01IW2',10000,1,'indiamutual',''),('335ea05a3d3594e7253f10c81ba397bb','NSE','SUPREMEIND','Supreme Industries Limited','INE195A01028',1,0,NULL,''),('447cde22fd642a46019ba30084fc4dfc','FUND','119071','DSPBR Midcap Direct-G','INF740K01PX1',10000,1,'amfiindia',''),('4afbe733c6ccea5f63fa4d4b12bb4b62','NSE','HDFCBANK','HDFC Bank Limited','INE040A01026',1,0,NULL,''),('7c97e2677a6ddae83a78313fd3252f5c','NSE','GRUH','GRUH Finance Ltd.','INE580B01029',1,0,NULL,''),('854c51f2837b3c49b130a5792d1dbc11','FUND','105804','Aditya Birla Sun Life Small & Midcap Fund - GROWTH','INF209K01EN2',10000,1,'amfiindia',''),('9f057231ab9513897ca01f565581ca57','FUND','120323','ICICI Prudential Value Discovery Fund - Direct Plan - Growth','INF109K012K1',10000,1,'indiamutual',''),('b417a83b048dbde8ef39c41ba0f0b0ce','CURRENCY','INR','Indian Rupee','356',100,1,'currency',''),('bc5b6e653b7fa594bb97c8fefe42b244','FUND','118525','Franklin India Smaller Companies Fund - Direct - Growth','INF090I01IQ4',10000,1,'amfiindia',''),('d413dfa6bef977f2cf0b7510f8e2a97c','NSE','EVERESTIND','Everest Industries Limited','INE295A01018',1,0,NULL,''),('d60159942c6afeca3dec68b68dbd0593','NSE','LICHSGFIN','LIC Housing Finance Limited','INE115A01026',1,0,NULL,''),('dd2ee5010f2d5c800af4a93e85c39a81','FUND','118834','Mirae Asset Emerging Bluechip Fund - Direct Plan - Growth','INF769K01BI1',10000,1,'amfiindia',''),('e21e7533eb970899b50177c0c50ce0b2','NSE','UJJIVAN','Ujjivan Financial Services Limited','INE334L01012',1,0,NULL,'');
/*!40000 ALTER TABLE `commodities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `guid` varchar(32) NOT NULL,
  `name` varchar(2048) NOT NULL,
  `id` varchar(2048) NOT NULL,
  `notes` varchar(2048) NOT NULL,
  `active` int(11) NOT NULL,
  `discount_num` bigint(20) NOT NULL,
  `discount_denom` bigint(20) NOT NULL,
  `credit_num` bigint(20) NOT NULL,
  `credit_denom` bigint(20) NOT NULL,
  `currency` varchar(32) NOT NULL,
  `tax_override` int(11) NOT NULL,
  `addr_name` varchar(1024) DEFAULT NULL,
  `addr_addr1` varchar(1024) DEFAULT NULL,
  `addr_addr2` varchar(1024) DEFAULT NULL,
  `addr_addr3` varchar(1024) DEFAULT NULL,
  `addr_addr4` varchar(1024) DEFAULT NULL,
  `addr_phone` varchar(128) DEFAULT NULL,
  `addr_fax` varchar(128) DEFAULT NULL,
  `addr_email` varchar(256) DEFAULT NULL,
  `shipaddr_name` varchar(1024) DEFAULT NULL,
  `shipaddr_addr1` varchar(1024) DEFAULT NULL,
  `shipaddr_addr2` varchar(1024) DEFAULT NULL,
  `shipaddr_addr3` varchar(1024) DEFAULT NULL,
  `shipaddr_addr4` varchar(1024) DEFAULT NULL,
  `shipaddr_phone` varchar(128) DEFAULT NULL,
  `shipaddr_fax` varchar(128) DEFAULT NULL,
  `shipaddr_email` varchar(256) DEFAULT NULL,
  `terms` varchar(32) DEFAULT NULL,
  `tax_included` int(11) DEFAULT NULL,
  `taxtable` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `guid` varchar(32) NOT NULL,
  `username` varchar(2048) NOT NULL,
  `id` varchar(2048) NOT NULL,
  `language` varchar(2048) NOT NULL,
  `acl` varchar(2048) NOT NULL,
  `active` int(11) NOT NULL,
  `currency` varchar(32) NOT NULL,
  `ccard_guid` varchar(32) DEFAULT NULL,
  `workday_num` bigint(20) NOT NULL,
  `workday_denom` bigint(20) NOT NULL,
  `rate_num` bigint(20) NOT NULL,
  `rate_denom` bigint(20) NOT NULL,
  `addr_name` varchar(1024) DEFAULT NULL,
  `addr_addr1` varchar(1024) DEFAULT NULL,
  `addr_addr2` varchar(1024) DEFAULT NULL,
  `addr_addr3` varchar(1024) DEFAULT NULL,
  `addr_addr4` varchar(1024) DEFAULT NULL,
  `addr_phone` varchar(128) DEFAULT NULL,
  `addr_fax` varchar(128) DEFAULT NULL,
  `addr_email` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entries`
--

DROP TABLE IF EXISTS `entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entries` (
  `guid` varchar(32) NOT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_entered` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `description` varchar(2048) DEFAULT NULL,
  `action` varchar(2048) DEFAULT NULL,
  `notes` varchar(2048) DEFAULT NULL,
  `quantity_num` bigint(20) DEFAULT NULL,
  `quantity_denom` bigint(20) DEFAULT NULL,
  `i_acct` varchar(32) DEFAULT NULL,
  `i_price_num` bigint(20) DEFAULT NULL,
  `i_price_denom` bigint(20) DEFAULT NULL,
  `i_discount_num` bigint(20) DEFAULT NULL,
  `i_discount_denom` bigint(20) DEFAULT NULL,
  `invoice` varchar(32) DEFAULT NULL,
  `i_disc_type` varchar(2048) DEFAULT NULL,
  `i_disc_how` varchar(2048) DEFAULT NULL,
  `i_taxable` int(11) DEFAULT NULL,
  `i_taxincluded` int(11) DEFAULT NULL,
  `i_taxtable` varchar(32) DEFAULT NULL,
  `b_acct` varchar(32) DEFAULT NULL,
  `b_price_num` bigint(20) DEFAULT NULL,
  `b_price_denom` bigint(20) DEFAULT NULL,
  `bill` varchar(32) DEFAULT NULL,
  `b_taxable` int(11) DEFAULT NULL,
  `b_taxincluded` int(11) DEFAULT NULL,
  `b_taxtable` varchar(32) DEFAULT NULL,
  `b_paytype` int(11) DEFAULT NULL,
  `billable` int(11) DEFAULT NULL,
  `billto_type` int(11) DEFAULT NULL,
  `billto_guid` varchar(32) DEFAULT NULL,
  `order_guid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entries`
--

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gnclock`
--

DROP TABLE IF EXISTS `gnclock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gnclock` (
  `Hostname` varchar(255) DEFAULT NULL,
  `PID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gnclock`
--

LOCK TABLES `gnclock` WRITE;
/*!40000 ALTER TABLE `gnclock` DISABLE KEYS */;
/*!40000 ALTER TABLE `gnclock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `guid` varchar(32) NOT NULL,
  `id` varchar(2048) NOT NULL,
  `date_opened` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `date_posted` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `notes` varchar(2048) NOT NULL,
  `active` int(11) NOT NULL,
  `currency` varchar(32) NOT NULL,
  `owner_type` int(11) DEFAULT NULL,
  `owner_guid` varchar(32) DEFAULT NULL,
  `terms` varchar(32) DEFAULT NULL,
  `billing_id` varchar(2048) DEFAULT NULL,
  `post_txn` varchar(32) DEFAULT NULL,
  `post_lot` varchar(32) DEFAULT NULL,
  `post_acc` varchar(32) DEFAULT NULL,
  `billto_type` int(11) DEFAULT NULL,
  `billto_guid` varchar(32) DEFAULT NULL,
  `charge_amt_num` bigint(20) DEFAULT NULL,
  `charge_amt_denom` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `guid` varchar(32) NOT NULL,
  `id` varchar(2048) NOT NULL,
  `name` varchar(2048) NOT NULL,
  `reference` varchar(2048) NOT NULL,
  `active` int(11) NOT NULL,
  `owner_type` int(11) DEFAULT NULL,
  `owner_guid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lots`
--

DROP TABLE IF EXISTS `lots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lots` (
  `guid` varchar(32) NOT NULL,
  `account_guid` varchar(32) DEFAULT NULL,
  `is_closed` int(11) NOT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lots`
--

LOCK TABLES `lots` WRITE;
/*!40000 ALTER TABLE `lots` DISABLE KEYS */;
/*!40000 ALTER TABLE `lots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `guid` varchar(32) NOT NULL,
  `id` varchar(2048) NOT NULL,
  `notes` varchar(2048) NOT NULL,
  `reference` varchar(2048) NOT NULL,
  `active` int(11) NOT NULL,
  `date_opened` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_closed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `owner_type` int(11) NOT NULL,
  `owner_guid` varchar(32) NOT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prices`
--

DROP TABLE IF EXISTS `prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prices` (
  `guid` varchar(32) NOT NULL,
  `commodity_guid` varchar(32) NOT NULL,
  `currency_guid` varchar(32) NOT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `source` varchar(2048) DEFAULT NULL,
  `type` varchar(2048) DEFAULT NULL,
  `value_num` bigint(20) NOT NULL,
  `value_denom` bigint(20) NOT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prices`
--

LOCK TABLES `prices` WRITE;
/*!40000 ALTER TABLE `prices` DISABLE KEYS */;
INSERT INTO `prices` VALUES ('079bc198fd85d053082ea7f9d1c695b7','dd2ee5010f2d5c800af4a93e85c39a81','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 06:30:00','Finance::Quote','nav',531360000,10000000),('1106576c75e289f1e2b535712267b1c8','4afbe733c6ccea5f63fa4d4b12bb4b62','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 18:30:00','user:price-editor','last',7907,4),('13e2f7ba78270886326eed031cf7624a','447cde22fd642a46019ba30084fc4dfc','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-04 06:30:00','Finance::Quote','nav',598090000,10000000),('1592995b9636c1b5cc8a6437c249084a','019fc6a229547340923551eea7019f6d','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-09 06:30:00','Finance::Quote','nav',407830000,10000000),('1fbc226b3fde1874a32f3fab7a96ea55','dd2ee5010f2d5c800af4a93e85c39a81','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-07 06:30:00','Finance::Quote','nav',530390000,10000000),('24dac8a306784ad09d1159818c7856a5','9f057231ab9513897ca01f565581ca57','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 06:30:00','Finance::Quote','nav',152990000,1000000),('2c51578afa11e7819226ff3ae26ffe5b','bc5b6e653b7fa594bb97c8fefe42b244','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 06:30:00','Finance::Quote','nav',651409000,10000000),('30e274bbbe0c0be7733ac24b0d043cce','bc5b6e653b7fa594bb97c8fefe42b244','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-07 06:30:00','Finance::Quote','nav',649563000,10000000),('3f30976b122a05e542fb1292d8ec627e','019fc6a229547340923551eea7019f6d','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-07 06:30:00','Finance::Quote','nav',404501000,10000000),('456e5f4d51cfcd8013e4cde9399816ec','d413dfa6bef977f2cf0b7510f8e2a97c','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 18:30:00','user:price-editor','last',9641,20),('49aa5cc74ea62b5f67fe34947fe22450','e21e7533eb970899b50177c0c50ce0b2','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 18:30:00','user:price-editor','last',4111,10),('4bcf5d66a2f452c2fa34aff9a05583bc','019fc6a229547340923551eea7019f6d','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 06:30:00','Finance::Quote','nav',407964000,10000000),('60f3cde9db114c879b5e6405abbf5382','854c51f2837b3c49b130a5792d1dbc11','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-04 06:30:00','Finance::Quote','nav',420432000,10000000),('64a50af55339f67863a8970a41ab363e','bc5b6e653b7fa594bb97c8fefe42b244','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-09 06:30:00','Finance::Quote','nav',650052000,10000000),('64ab70545a6c6ae5c3d5c823e6576537','447cde22fd642a46019ba30084fc4dfc','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-09 06:30:00','Finance::Quote','nav',598730000,10000000),('663df306c87666d4e6de9dd4df3d4b20','7c97e2677a6ddae83a78313fd3252f5c','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 18:30:00','user:price-editor','last',3454,5),('6b49cc7c5315b8ae3a3620df1801f9e2','9f057231ab9513897ca01f565581ca57','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-04 06:30:00','Finance::Quote','nav',151770000,1000000),('6eaf0fd12e5392b6988516b8224bef70','9f057231ab9513897ca01f565581ca57','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-09 06:30:00','Finance::Quote','nav',152640000,1000000),('72655282338b5f607a2a88f38eea64ac','dd2ee5010f2d5c800af4a93e85c39a81','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-09 18:30:00','user:price',NULL,53060015,1000000),('8e46c37bc7a740644720e06b3ff24c20','854c51f2837b3c49b130a5792d1dbc11','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-09 06:30:00','Finance::Quote','nav',425397000,10000000),('9225d753c8dea2e3ff2706bb5ba3a1a8','bc5b6e653b7fa594bb97c8fefe42b244','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-04 06:30:00','Finance::Quote','nav',648659000,10000000),('b2c0a9b4c637855f32243f6a6af0b8a9','335ea05a3d3594e7253f10c81ba397bb','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 18:30:00','user:price-editor','last',1345,1),('be3dfe84e0aadd8cf72e5b5803de3146','dd2ee5010f2d5c800af4a93e85c39a81','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-09 06:30:00','Finance::Quote','nav',530600000,10000000),('c67652b8731120deab6042b1a9b81988','019fc6a229547340923551eea7019f6d','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-04 06:30:00','Finance::Quote','nav',400526000,10000000),('d38b5e67f76a218754f3768215d1ea7b','d60159942c6afeca3dec68b68dbd0593','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 18:30:00','user:price-editor','last',5087,10),('d5124f647738950d74fb9526d3e6d24a','9f057231ab9513897ca01f565581ca57','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-07 06:30:00','Finance::Quote','nav',153080000,1000000),('e477c39d78dc68504c4c4d487d68791f','854c51f2837b3c49b130a5792d1dbc11','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-07 06:30:00','Finance::Quote','nav',423657000,10000000),('e577e140544d41ff27c8671c8a974b27','dd2ee5010f2d5c800af4a93e85c39a81','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-04 06:30:00','Finance::Quote','nav',525940000,10000000),('ebbd127d7cd9aa70893d72c1a98914ab','854c51f2837b3c49b130a5792d1dbc11','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 06:30:00','Finance::Quote','nav',425471000,10000000),('f3649229475bd270046e2cdc215213cc','447cde22fd642a46019ba30084fc4dfc','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-07 06:30:00','Finance::Quote','nav',600620000,10000000),('f4019d4d61c576eb907eb050e8b96765','447cde22fd642a46019ba30084fc4dfc','b417a83b048dbde8ef39c41ba0f0b0ce','2018-05-08 06:30:00','Finance::Quote','nav',603720000,10000000);
/*!40000 ALTER TABLE `prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recurrences`
--

DROP TABLE IF EXISTS `recurrences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recurrences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `obj_guid` varchar(32) NOT NULL,
  `recurrence_mult` int(11) NOT NULL,
  `recurrence_period_type` varchar(2048) NOT NULL,
  `recurrence_period_start` date NOT NULL,
  `recurrence_weekend_adjust` varchar(2048) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recurrences`
--

LOCK TABLES `recurrences` WRITE;
/*!40000 ALTER TABLE `recurrences` DISABLE KEYS */;
/*!40000 ALTER TABLE `recurrences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedxactions`
--

DROP TABLE IF EXISTS `schedxactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedxactions` (
  `guid` varchar(32) NOT NULL,
  `name` varchar(2048) DEFAULT NULL,
  `enabled` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `last_occur` date DEFAULT NULL,
  `num_occur` int(11) NOT NULL,
  `rem_occur` int(11) NOT NULL,
  `auto_create` int(11) NOT NULL,
  `auto_notify` int(11) NOT NULL,
  `adv_creation` int(11) NOT NULL,
  `adv_notify` int(11) NOT NULL,
  `instance_count` int(11) NOT NULL,
  `template_act_guid` varchar(32) NOT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedxactions`
--

LOCK TABLES `schedxactions` WRITE;
/*!40000 ALTER TABLE `schedxactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedxactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slots`
--

DROP TABLE IF EXISTS `slots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `obj_guid` varchar(32) NOT NULL,
  `name` varchar(4096) NOT NULL,
  `slot_type` int(11) NOT NULL,
  `int64_val` bigint(20) DEFAULT NULL,
  `string_val` varchar(4096) DEFAULT NULL,
  `double_val` double DEFAULT NULL,
  `timespec_val` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `guid_val` varchar(32) DEFAULT NULL,
  `numeric_val_num` bigint(20) DEFAULT NULL,
  `numeric_val_denom` bigint(20) DEFAULT NULL,
  `gdate_val` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `slots_guid_index` (`obj_guid`)
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slots`
--

LOCK TABLES `slots` WRITE;
/*!40000 ALTER TABLE `slots` DISABLE KEYS */;
INSERT INTO `slots` VALUES (1,'7505cd9c8cb2fc8127777312c8198d9d','options',9,0,NULL,0,NULL,'a5f46f9688674f4f0f38fb6faf3c314f',0,1,NULL),(2,'a5f46f9688674f4f0f38fb6faf3c314f','options/Budgeting',9,0,NULL,0,NULL,'c82f9b4a6e42a8943c80ab3f3ce744af',0,1,NULL),(3,'7505cd9c8cb2fc8127777312c8198d9d','counter_formats',9,0,NULL,0,NULL,'8912fdb12dabea2003964764ca9249a4',0,1,NULL),(4,'2e05ce283fee5ae9134493d71849e4e2','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(5,'54dfcc64b77c1d1c11449174f1716912','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(6,'c880925e86f2d464085bd468e04d3b1b','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(7,'c880925e86f2d464085bd468e04d3b1b','notes',4,0,'IRA, 401(k), or other retirement',0,NULL,NULL,0,1,NULL),(8,'aebb22fc50169e1a556deaa898a3337c','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(9,'aebb22fc50169e1a556deaa898a3337c','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(10,'a1164e62e7bd81d0e7c52701847f3e8f','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(11,'a1164e62e7bd81d0e7c52701847f3e8f','notes',4,0,'EPFO Self Contribution',0,NULL,NULL,0,1,NULL),(12,'a1164e62e7bd81d0e7c52701847f3e8f','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(13,'446050a5d3b0efa8a78803a133808b8c','notes',4,0,'EPFO - Employee Contribution',0,NULL,NULL,0,1,NULL),(14,'446050a5d3b0efa8a78803a133808b8c','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(15,'203ea0bbbf9cae3fc40bd8aa81e43caf','notes',4,0,'EPFO - Employee Contribution',0,NULL,NULL,0,1,NULL),(16,'203ea0bbbf9cae3fc40bd8aa81e43caf','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(17,'2da96fc6d7160ed1faa3530e011ac6ae','notes',4,0,'EPFO - Pension Contribution',0,NULL,NULL,0,1,NULL),(18,'2da96fc6d7160ed1faa3530e011ac6ae','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(19,'4ce88194ae2bf4414fbaa5f7972a724e','notes',4,0,'PPF Account',0,NULL,NULL,0,1,NULL),(20,'4ce88194ae2bf4414fbaa5f7972a724e','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(21,'13e00c9b7ba95740fcebf1285328bf1a','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(22,'13e00c9b7ba95740fcebf1285328bf1a','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(23,'d413dfa6bef977f2cf0b7510f8e2a97c','user_symbol',4,0,'EVERESTIND',0,NULL,NULL,0,1,NULL),(24,'127a7bccdea13690d45abef992b2779f','notes',4,0,'Everest Industries Limited',0,NULL,NULL,0,1,NULL),(25,'127a7bccdea13690d45abef992b2779f','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(26,'4afbe733c6ccea5f63fa4d4b12bb4b62','user_symbol',4,0,'HDFCBANK',0,NULL,NULL,0,1,NULL),(27,'193f6f2a73ce82c9cc453dd599273b6c','notes',4,0,'HDFC Bank Limited',0,NULL,NULL,0,1,NULL),(28,'193f6f2a73ce82c9cc453dd599273b6c','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(29,'d60159942c6afeca3dec68b68dbd0593','user_symbol',4,0,'LICHSGFIN',0,NULL,NULL,0,1,NULL),(30,'925544064b77d21245b385e13c252d84','notes',4,0,'LIC Housing Finance Limited',0,NULL,NULL,0,1,NULL),(31,'925544064b77d21245b385e13c252d84','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(32,'0b50a16343d5e07190dfa91d0fe91355','notes',4,0,'Supreme Industries Ltd',0,NULL,NULL,0,1,NULL),(33,'0b50a16343d5e07190dfa91d0fe91355','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(34,'58128fb41bfd11077b336535fdb2f407','notes',4,0,'Ujjivan Financial Services Ltd',0,NULL,NULL,0,1,NULL),(35,'58128fb41bfd11077b336535fdb2f407','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(36,'946bf0729b8cdf2db077b8b4df60fa3e','notes',4,0,'GRUH Finance Ltd.',0,NULL,NULL,0,1,NULL),(37,'946bf0729b8cdf2db077b8b4df60fa3e','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(38,'9f057231ab9513897ca01f565581ca57','user_symbol',4,0,'120323',0,NULL,NULL,0,1,NULL),(39,'1edf8a7638e59599acbbe57ac24f7a8f','notes',4,0,'ICICI Pru Value Discovery Direct-G',0,NULL,NULL,0,1,NULL),(40,'1edf8a7638e59599acbbe57ac24f7a8f','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(41,'e7db8aaedf902655b44dc027ee03bb60','notes',4,0,'Aditya Birla SL Small & Midcap Direct-G',0,NULL,NULL,0,1,NULL),(42,'e7db8aaedf902655b44dc027ee03bb60','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(43,'eceb6509415a2c2ffab48041ca9d7edb','notes',4,0,'DSPBR Midcap Direct-G',0,NULL,NULL,0,1,NULL),(44,'eceb6509415a2c2ffab48041ca9d7edb','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(45,'019fc6a229547340923551eea7019f6d','user_symbol',4,0,'118564',0,NULL,NULL,0,1,NULL),(46,'f97a1cc92bbd40fd14d13b44490cc770','notes',4,0,'Franklin India High Growth Companies Direct-G',0,NULL,NULL,0,1,NULL),(47,'f97a1cc92bbd40fd14d13b44490cc770','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(48,'7253248269cfd24da0a6c3ac94cf4d8a','notes',4,0,'Franklin India Smaller Companies Direct-G',0,NULL,NULL,0,1,NULL),(49,'7253248269cfd24da0a6c3ac94cf4d8a','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(50,'dd2ee5010f2d5c800af4a93e85c39a81','user_symbol',4,0,'118834',0,NULL,NULL,0,1,NULL),(51,'6ddda8e5c5fe1717377da46c70b2d66a','notes',4,0,'Mirae Asset Emerging Bluechip Direct-G',0,NULL,NULL,0,1,NULL),(52,'6ddda8e5c5fe1717377da46c70b2d66a','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(53,'e51c1f47b657d70f26bf160e30b35354','notes',4,0,'Brokerage Margin',0,NULL,NULL,0,1,NULL),(54,'e51c1f47b657d70f26bf160e30b35354','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(55,'e0b8b5011fa1489874e9f3033b6a4806','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(56,'e0b8b5011fa1489874e9f3033b6a4806','reconcile-info',9,0,NULL,0,NULL,'8134acd1f5e98921f0574f21b8e75660',0,1,NULL),(57,'8134acd1f5e98921f0574f21b8e75660','reconcile-info/last-date',1,1525112999,NULL,0,NULL,NULL,0,1,NULL),(58,'8134acd1f5e98921f0574f21b8e75660','reconcile-info/include-children',1,0,NULL,0,NULL,NULL,0,1,NULL),(59,'c36b3ebdd1e1701486319297cc13fe64','notes',4,0,'Paytm Wallet.',0,NULL,NULL,0,1,NULL),(60,'c36b3ebdd1e1701486319297cc13fe64','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(61,'cb2d2284961367f8f95df33f9e2cde6b','notes',4,0,'Food Coupons',0,NULL,NULL,0,1,NULL),(62,'cb2d2284961367f8f95df33f9e2cde6b','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(63,'ae8891bb416c6908b1e5c1e9c2c42660','notes',4,0,'Intermediary Account',0,NULL,NULL,0,1,NULL),(64,'ae8891bb416c6908b1e5c1e9c2c42660','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(65,'2c1729025ff9cb85c4ee772511682ed6','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(66,'2c1729025ff9cb85c4ee772511682ed6','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(67,'5242f3b0472b95d88ffc7c548b11e3d6','notes',4,0,'Name : Mr. VENKATA UDAY KIRAN S\nDescription : SBCHQ-CSA-PUB-IND-CSPLT-INR',0,NULL,NULL,0,1,NULL),(68,'5242f3b0472b95d88ffc7c548b11e3d6','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(69,'0d21fec3e0fcb1619dc17f6fe139efc8','notes',4,0,'name : Mrs. ANUSHA NAGENDLA',0,NULL,NULL,0,1,NULL),(70,'0d21fec3e0fcb1619dc17f6fe139efc8','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(71,'3d87a9a6fa3c92191e146c44115f0c07','notes',4,0,'HDFC Bank Savings Account',0,NULL,NULL,0,1,NULL),(72,'3d87a9a6fa3c92191e146c44115f0c07','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(73,'27c4224f38bff085ef3bf0d61833e522','notes',4,0,'ICICI Bank Account',0,NULL,NULL,0,1,NULL),(74,'27c4224f38bff085ef3bf0d61833e522','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(75,'ee3a5de26dcbed105dbcbc629fd54612','notes',4,0,'SBI-HomeLoan-Investment',0,NULL,NULL,0,1,NULL),(76,'ee3a5de26dcbed105dbcbc629fd54612','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(77,'49cf17633d5acb5452d239caa8009cb9','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(78,'9fc2fff884cb47b1b9c14aac3d73ba85','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(79,'9271ca1a07bf91fa11a6ec02d984939d','notes',4,0,'name : Mrs. ANUSHA NAGENDLA',0,NULL,NULL,0,1,NULL),(80,'9271ca1a07bf91fa11a6ec02d984939d','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(81,'63ab6c713fe80a46064ad97783fddbcd','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(82,'63ab6c713fe80a46064ad97783fddbcd','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(83,'3f3a7895f1bcd173de594539b1e12c48','notes',4,0,'SBI Card',0,NULL,NULL,0,1,NULL),(84,'3f3a7895f1bcd173de594539b1e12c48','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(85,'8fc91fbe3db81364bd56d3d4a4853807','notes',4,0,'Citibank Cards',0,NULL,NULL,0,1,NULL),(86,'8fc91fbe3db81364bd56d3d4a4853807','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(87,'46fbf77c4e9831aa218b6611654c4c73','notes',4,0,'HDFC Bank',0,NULL,NULL,0,1,NULL),(88,'46fbf77c4e9831aa218b6611654c4c73','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(89,'b8e7452aafccd6e4d0db1484c0d55804','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(90,'ffe550729c1a98aa720c4b528afbdc8e','notes',4,0,'Bike',0,NULL,NULL,0,1,NULL),(91,'ffe550729c1a98aa720c4b528afbdc8e','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(92,'7216687f8a712f14af685716d2eead70','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(93,'e25012118b8a86970898b0fe2fcea20a','notes',4,0,'OlaCab',0,NULL,NULL,0,1,NULL),(94,'e25012118b8a86970898b0fe2fcea20a','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(95,'6965f0a52e99ed48269f9b7f60797f42','notes',4,0,'UberCab',0,NULL,NULL,0,1,NULL),(96,'6965f0a52e99ed48269f9b7f60797f42','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(97,'b5add894133fa37d07ea514ecd8ccf7e','notes',4,0,'Train-IRCTC',0,NULL,NULL,0,1,NULL),(98,'b5add894133fa37d07ea514ecd8ccf7e','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(99,'9db15a2d6747ef2d7c09708f8af91d97','notes',4,0,'Other',0,NULL,NULL,0,1,NULL),(100,'9db15a2d6747ef2d7c09708f8af91d97','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(101,'9985a704f5836e1c364237bd30f7de11','notes',4,0,'Central Income Tax',0,NULL,NULL,0,1,NULL),(102,'9985a704f5836e1c364237bd30f7de11','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(103,'107f66386bd5dc23397ca95df4253044','notes',4,0,'Central GST',0,NULL,NULL,0,1,NULL),(104,'107f66386bd5dc23397ca95df4253044','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(105,'0e31f81088bbdcba65ea21fe1900aa25','notes',4,0,'SGST',0,NULL,NULL,0,1,NULL),(106,'0e31f81088bbdcba65ea21fe1900aa25','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(107,'fe879ccb6fe227e284eb043f6a3998ef','notes',4,0,'House Tax',0,NULL,NULL,0,1,NULL),(108,'fe879ccb6fe227e284eb043f6a3998ef','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(109,'f36e7e8b06eb3e567186b1b7148208e5','notes',4,0,'Professional Tax',0,NULL,NULL,0,1,NULL),(110,'f36e7e8b06eb3e567186b1b7148208e5','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(111,'cb41053e15d88ee4c22d0320b84e9519','notes',4,0,'Electricity Payments',0,NULL,NULL,0,1,NULL),(112,'cb41053e15d88ee4c22d0320b84e9519','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(113,'88ec26a19ae851cfbaafcd1b42f6ee42','notes',4,0,'Phone',0,NULL,NULL,0,1,NULL),(114,'88ec26a19ae851cfbaafcd1b42f6ee42','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(115,'72fb0f4ab210fc19df29895461406374','notes',4,0,'Dining',0,NULL,NULL,0,1,NULL),(116,'72fb0f4ab210fc19df29895461406374','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(117,'422a30a247933efb47b17e9638722da7','notes',4,0,'Snacks',0,NULL,NULL,0,1,NULL),(118,'422a30a247933efb47b17e9638722da7','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(119,'caf3feba0afc6f0b9f7bce678b56f392','notes',4,0,'Restaurant Dining',0,NULL,NULL,0,1,NULL),(120,'caf3feba0afc6f0b9f7bce678b56f392','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(121,'3c237ec529e2c2c313ca60e2129dabcc','notes',4,0,'Groceries',0,NULL,NULL,0,1,NULL),(122,'3c237ec529e2c2c313ca60e2129dabcc','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(123,'98b87123589a93713b98c61d5276baa5','notes',4,0,'Groceries',0,NULL,NULL,0,1,NULL),(124,'98b87123589a93713b98c61d5276baa5','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(125,'884670abc0e614e6c2ba581ec33926d9','notes',4,0,'Fruits',0,NULL,NULL,0,1,NULL),(126,'884670abc0e614e6c2ba581ec33926d9','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(127,'5986ada838a621cd06fae5c027fed6c7','notes',4,0,'Stationary',0,NULL,NULL,0,1,NULL),(128,'5986ada838a621cd06fae5c027fed6c7','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(129,'0863b4cc996155d0335bae2a743ee9c1','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(130,'0863b4cc996155d0335bae2a743ee9c1','notes',4,0,'Gifts',0,NULL,NULL,0,1,NULL),(131,'0863b4cc996155d0335bae2a743ee9c1','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(132,'0731bc05804a4e132fbfa611a4ea1138','notes',4,0,'Tithe-Lords Church',0,NULL,NULL,0,1,NULL),(133,'0731bc05804a4e132fbfa611a4ea1138','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(134,'7f1aec77ac35b0e3c92a22cac0db86e5','notes',4,0,'Tithe-IPHC Hyderabad',0,NULL,NULL,0,1,NULL),(135,'7f1aec77ac35b0e3c92a22cac0db86e5','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(136,'73e0d82638c6ab8a656009e2ecaa8dad','notes',4,0,'Ammamma-Gift',0,NULL,NULL,0,1,NULL),(137,'73e0d82638c6ab8a656009e2ecaa8dad','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(138,'7a58f1efc9a4125a2c483cd1eeccfb53','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(139,'7a58f1efc9a4125a2c483cd1eeccfb53','notes',4,0,'Maintenance',0,NULL,NULL,0,1,NULL),(140,'7a58f1efc9a4125a2c483cd1eeccfb53','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(141,'455af3d10bb5a2c5b9c905afa64bfe5d','notes',4,0,'Monthly Household Expeneses',0,NULL,NULL,0,1,NULL),(142,'455af3d10bb5a2c5b9c905afa64bfe5d','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(143,'4b3e074809e45e9bc6e8d2184b6f10d3','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(144,'4b3e074809e45e9bc6e8d2184b6f10d3','notes',4,0,'Home Expenses',0,NULL,NULL,0,1,NULL),(145,'4b3e074809e45e9bc6e8d2184b6f10d3','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(146,'3fc86d569a9e7a27767ac24e7ec1aa18','notes',4,0,'Maintenance',0,NULL,NULL,0,1,NULL),(147,'3fc86d569a9e7a27767ac24e7ec1aa18','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(148,'a9e9ffb50d365ee4990c529fc6e81220','notes',4,0,'Misc expenses',0,NULL,NULL,0,1,NULL),(149,'a9e9ffb50d365ee4990c529fc6e81220','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(150,'cc4f98a8aeedef3908a5075f0b2840a5','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(151,'eba66f9e03bcfe6d853da967f6ec18ea','notes',4,0,'TeamF1 Networks Salary',0,NULL,NULL,0,1,NULL),(152,'eba66f9e03bcfe6d853da967f6ec18ea','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(153,'eee1570cb845f0f957c7ee5302905367','notes',4,0,'Cashback',0,NULL,NULL,0,1,NULL),(154,'eee1570cb845f0f957c7ee5302905367','color',4,0,'Not Set',0,NULL,NULL,0,1,NULL),(155,'1bc887b0cbfcae70284b8e099164bd22','placeholder',4,0,'true',0,NULL,NULL,0,1,NULL),(156,'649848eaf518be30a3f00ee5453bb59b','reconcile-info',9,0,NULL,0,NULL,'619f4cecde77aabc6c3083cb3a883ee9',0,1,NULL),(157,'619f4cecde77aabc6c3083cb3a883ee9','reconcile-info/include-children',1,0,NULL,0,NULL,NULL,0,1,NULL),(158,'81f23103d5e5a98b00be07548e472a33','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-27'),(159,'b6210aae4aacc9e210ab717d0adb4e2f','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(160,'fd734f0d66ae4b5a7473a79e7a0c36fb','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-27'),(161,'dbf57d84ba949e0823baf21cdd556215','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-27'),(162,'c63d571c8c8e731a43d215e6e37b85b1','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-30'),(163,'d30dab89b7f07bba5726a8113575b1c3','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(164,'2518dea74982a7924a8b86483ac4e154','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-09'),(165,'391e2eba383eb662996a033102cd5c89','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-09'),(166,'0d8b6ee871c09e25c1870ed203b99a4b','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-09'),(167,'945e0c9ac670dc879c3e7381ce41f0cb','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-09'),(168,'792989dd404b045b1649273db0ecc0ba','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-09'),(169,'08df1531f942dcbd48b18c27c177b541','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(170,'42ff6ba8306a357c810c726e3324db2b','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(171,'60bc46e0691530de731164d01e03a5c8','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(172,'dcdc54f084a3e7c9f652c7eedd36bc9a','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(173,'1d83712d01f2de580a519c6fb52010f8','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(174,'c03b87e76255425bffa0aee7b7432ecd','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(175,'a63daec7afd9f4387618202ef3f8e98f','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(176,'91e3dec0025b93ab696ed3c37d2202c2','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(177,'62c5fd98887b1bbb11462bf2021a518f','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(178,'59f4bb98508a81d730009c9c9d9281f6','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-10'),(179,'fd9aed71b43ae11435158c8b037870e5','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(180,'a822ced31614de312c17f87cffad2720','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(181,'d904fe0c86e03a40bece802dda7c6885','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-30'),(182,'c6ea891f146f6b749947213e07a0a491','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-30'),(183,'fe962033a19efdbccee0c953f9898847','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(184,'d572244f4e92bccd038ded7981fc2284','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(185,'30f98796d9b773a4c19050430d1ac7c2','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(186,'6a4d6d5179e7baab0e120bf1749c63a1','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-02'),(187,'b8b687353c0e1d3fe7914eed36e432bc','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-02'),(188,'3e19e3e8ef011106186d7cd5dcc22819','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-02'),(189,'3cb47cbc3c815e2def535b38d3ab66a0','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-02'),(190,'65d31498732bac9bfb03cb8cf4893d86','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-02'),(191,'84a926ab65fef1920a76aeb60106516b','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-03'),(192,'abef812dac03dc03072da2f0829cba40','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-03'),(193,'683502cd128985e6b85e20db695629f1','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-04'),(194,'10083688ab53aa6c23e1fc9bf2c28761','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-04'),(195,'cdf6b1f03b5040be52e1511d6ddcdedf','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-04'),(196,'6971c041bd3d2583ffa1f07679e47306','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-06'),(197,'d569b17226d399954a8f6b0b59bbfe44','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-06'),(198,'f059491c0ad999312a4baa6e8421f48e','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(199,'4c387a5f62a7338a0c40e4dc208f81db','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(200,'f357b8429d00dcc3c2bd8cca6b13fba6','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(201,'933c00398c12be75d13db6ce8ab3f2a8','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-09'),(202,'e63cdfe119cfdec2670f172232716884','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-09'),(203,'ea2daae2bd2dd1cc490524dc4dab749b','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-09'),(205,'165a027f497ced4cac8204a8750b0264','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-30'),(206,'f666cb97b1e66890750571206a9203ce','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-03'),(207,'4af5764509cdcf82b33ec5febdbd2bf8','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-03'),(208,'d2e411ee24bedef4782853d8edc48f21','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-03'),(209,'a74dc73a07af92341421ee55eb964242','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-04'),(210,'2a18231dfd6da62c7642b64c0235755c','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-05'),(211,'8f3fe30c92b8f02177cf3632e2ef34e3','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(212,'18af0ba8c6b789f967ca19abd323c7ce','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(213,'3359352250a3097ec431860d34655d0f','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(214,'077078494de85727f7f630b0c9106802','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(215,'e32066d2382acb1c3fae620f5f0d4e60','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-09'),(216,'6389ab8d436c597b76bc0b2bd7f4b03c','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(217,'1b6523e97820b8516d866dd7c0ea0920','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(218,'3de63b7a2bcbc128c53dc1b2a69cc00d','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(219,'ab873d23fde9c394dff336e2e37eaaa5','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(220,'e9409f498abb73587d6d06a26fec5431','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(221,'8a73255769dde9e94cafaf4e1ba7082a','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(222,'c17624c125501cf4634118e9931b278b','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(223,'86faf8409a4b93930063f924bb3437e4','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(224,'bbc651b16602f69d6f3de3c4663cb142','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(225,'6d00d5f5fdc8a086f716869413c67562','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(226,'afb57cee35f8c9f8bb1be6cab4fbc376','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-27'),(227,'6736e94a8e4a90f613fedec0bd8b9fb5','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(228,'21c772bd72196c4770e2996d402c30f4','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(229,'f8e204f029f89df492791072eacb4084','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(230,'0761b5918a996dd4698938b34c269e05','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-02'),(231,'bab31494c317629328eccbe5c2b91b31','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-27'),(232,'5945071f283e32e4704c93f6e47945e4','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(233,'db47fca4525907d72f8eb3bc8135324e','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-27'),(234,'f2858a1f6ab1ee9d1a61c7e39f88e7d4','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-27'),(235,'e27f878d73618f45013d4b69deab9f33','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-29'),(236,'c320c609d9985ca62fe1efae83662675','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-29'),(237,'10cc11e31906bedf14896888d2520441','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-29'),(238,'7ea561b11ab354939e46aa10c63443b2','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-29'),(239,'a1fd051dc8f4554b92e351be4834ef84','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-29'),(240,'589c9881dceaaad9f4ab7130474aec00','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-29'),(241,'3f67008a0d820787b8602ae8d736cf55','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-29'),(242,'33d6570a5732f1990927e51a627259aa','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(243,'575c25f6de16c57579d0f8c1ea8167ce','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(244,'f368661db658a8e87a7baf4774ef47ae','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(245,'328d61f023ae899356dbc0a08c0355b2','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-02'),(246,'803a63c0f17cc5ba99bbc2ad7511388f','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-06'),(247,'bae9df9759974fd8225ddcea9a3ecf46','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-07'),(248,'b8b7d27ab5a4b167b1f46cf1b899b0d9','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-08'),(249,'48b6e9d480ef84f5896f7f89d1984542','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-09'),(250,'f72512e1ffe5b9c93418e1f8c4ad5065','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-27'),(251,'fc1553224a627c069c8a1bf2c178e039','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-23'),(252,'95a69367d8b657654229cdc3910905db','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-30'),(253,'fae62e48fcb791386c9409aadab2d108','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-27'),(254,'d2369d52fb1dfde26f27faea109a0ea7','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(255,'3756375f95b9aacbc8166327da21b77c','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-04-30'),(256,'b6e8e40b5f99f424bf9399c336e4eb7c','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-01'),(260,'46088982285bbcb0ac415e1018c9d09d','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-10'),(261,'bf3d2238ea6885ab0a9e625d032b7d03','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-10'),(262,'be17549d5b039e84a2b2cced4548abf9','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-10'),(263,'48a649b82343eac16a5a08efa1f84903','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-10'),(264,'c9273d5810d3e5ba78d0126f66c678ea','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-10'),(266,'bdbc9e5820c8f9c1611da166a1e6d160','date-posted',10,0,NULL,0,NULL,NULL,0,1,'2018-05-10');
/*!40000 ALTER TABLE `slots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `splits`
--

DROP TABLE IF EXISTS `splits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `splits` (
  `guid` varchar(32) NOT NULL,
  `tx_guid` varchar(32) NOT NULL,
  `account_guid` varchar(32) NOT NULL,
  `memo` varchar(2048) NOT NULL,
  `action` varchar(2048) NOT NULL,
  `reconcile_state` varchar(1) NOT NULL,
  `reconcile_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `value_num` bigint(20) NOT NULL,
  `value_denom` bigint(20) NOT NULL,
  `quantity_num` bigint(20) NOT NULL,
  `quantity_denom` bigint(20) NOT NULL,
  `lot_guid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`guid`),
  KEY `splits_tx_guid_index` (`tx_guid`),
  KEY `splits_account_guid_index` (`account_guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `splits`
--

LOCK TABLES `splits` WRITE;
/*!40000 ALTER TABLE `splits` DISABLE KEYS */;
INSERT INTO `splits` VALUES ('0014a46da1191f7774edd1514e554d5f','91e3dec0025b93ab696ed3c37d2202c2','7253248269cfd24da0a6c3ac94cf4d8a','','','n',NULL,180000,100,277496,10000,NULL),('00389cc661ae38f97fc27889e6401dd6','c63d571c8c8e731a43d215e6e37b85b1','4ce88194ae2bf4414fbaa5f7972a724e','','','n',NULL,2902200,100,2902200,100,NULL),('03b4d152776edbd368eb708e3cf9d185','bbc651b16602f69d6f3de3c4663cb142','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,1000000,100,1000000,100,NULL),('04eeea0d569c8ce8311befd1dec281f4','2518dea74982a7924a8b86483ac4e154','193f6f2a73ce82c9cc453dd599273b6c','','Buy','n',NULL,196410,100,1,1,NULL),('05e6200cbdbc6d87120565c19f16c857','b6210aae4aacc9e210ab717d0adb4e2f','cb2d2284961367f8f95df33f9e2cde6b','Food Coupons','','n',NULL,200000,100,200000,100,NULL),('06c0e37ac0c29d45e6c5ab1a595a022f','6d00d5f5fdc8a086f716869413c67562','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,-500000,100,-500000,100,NULL),('08e15e6f9e713e747caeb58c1c6cd394','fe962033a19efdbccee0c953f9898847','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-3000,100,-3000,100,NULL),('09fb84cc459b08152004bd0062c5e416','e32066d2382acb1c3fae620f5f0d4e60','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,-3000,100,-3000,100,NULL),('0a05e54def85e745e79ad5a4a1e351cc','db47fca4525907d72f8eb3bc8135324e','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-848435,100,-848435,100,NULL),('0b676649a8d0ec0db040fd9286f26892','b6210aae4aacc9e210ab717d0adb4e2f','203ea0bbbf9cae3fc40bd8aa81e43caf','EPFO Employee contribution.','','n',NULL,1040000,100,1040000,100,NULL),('0b8e7464052f59a16071925d9a179583','803a63c0f17cc5ba99bbc2ad7511388f','107f66386bd5dc23397ca95df4253044','','','n',NULL,22200,100,22200,100,NULL),('0c9c78266a146202492a48d587a5a3e2','c6ea891f146f6b749947213e07a0a491','422a30a247933efb47b17e9638722da7','','','n',NULL,1500,100,1500,100,NULL),('0da6bb0d1952d5f75c0052a9ba5127f9','86faf8409a4b93930063f924bb3437e4','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,-500000,100,-500000,100,NULL),('0dbb19ba28c42467873b55cc644e3032','afb57cee35f8c9f8bb1be6cab4fbc376','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-93946,100,-93946,100,NULL),('0e002868b2d38c243cc2b8df6f1612f2','683502cd128985e6b85e20db695629f1','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-15000,100,-15000,100,NULL),('0e14bdc7fd493ec75e36c8f74b3f7452','d904fe0c86e03a40bece802dda7c6885','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-92000,100,-92000,100,NULL),('0f92eb4b7740a6606e0d36596f3666ab','dcdc54f084a3e7c9f652c7eedd36bc9a','eceb6509415a2c2ffab48041ca9d7edb','','Buy','n',NULL,400720,100,670000,10000,NULL),('10c18bff687095ad66a2f10f32a43003','c17624c125501cf4634118e9931b278b','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,-1000000,100,-1000000,100,NULL),('116a2e5573cad55cb18f3b631acca1c2','3de63b7a2bcbc128c53dc1b2a69cc00d','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,-2560000,100,-2560000,100,NULL),('1203beb4d5925d933ef190cf37ab6343','8f3fe30c92b8f02177cf3632e2ef34e3','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,-19500,100,-19500,100,NULL),('1425db9b45b443a09c536bebe16ff849','a1fd051dc8f4554b92e351be4834ef84','caf3feba0afc6f0b9f7bce678b56f392','','','n',NULL,80115,100,80115,100,NULL),('1456d6abff7468d58ffa6f790e6b9790','e9409f498abb73587d6d06a26fec5431','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,1500000,100,1500000,100,NULL),('14716e2a4ef5a0040ee160c1cb02e6d5','b6210aae4aacc9e210ab717d0adb4e2f','9985a704f5836e1c364237bd30f7de11','','','n',NULL,3646400,100,3646400,100,NULL),('14c48609d1de0cc783c45731219964a5','c9273d5810d3e5ba78d0126f66c678ea','a9e9ffb50d365ee4990c529fc6e81220','','','n',NULL,6000,100,6000,100,NULL),('17a1aa2441682d54233c00b67321a6bb','08df1531f942dcbd48b18c27c177b541','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-2200665,100,-2200665,100,NULL),('1a47bc1be7944119346edb48031336a5','b6210aae4aacc9e210ab717d0adb4e2f','2da96fc6d7160ed1faa3530e011ac6ae','','','n',NULL,125000,100,125000,100,NULL),('1ad736c884d6980b9d28254a8fe6533b','65d31498732bac9bfb03cb8cf4893d86','422a30a247933efb47b17e9638722da7','','','n',NULL,3000,100,3000,100,NULL),('1b44cfd8134e559f5a462ffdf4b19a28','b6210aae4aacc9e210ab717d0adb4e2f','446050a5d3b0efa8a78803a133808b8c','EPFO Employer contribution.','','n',NULL,915000,100,915000,100,NULL),('1bb3b4f2acf6f98a12744948d000bb92','18af0ba8c6b789f967ca19abd323c7ce','46fbf77c4e9831aa218b6611654c4c73','','','n',NULL,-20000,100,-20000,100,NULL),('1d686dcd3b8cc74c63abce5afc006f29','ab873d23fde9c394dff336e2e37eaaa5','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,-2560000,100,-2560000,100,NULL),('1fbbdaf117b1fb215d6bc095a0f3a995','933c00398c12be75d13db6ce8ab3f2a8','884670abc0e614e6c2ba581ec33926d9','','','n',NULL,4000,100,4000,100,NULL),('1ffba0bb9bf1d4152c0f1459efc4749f','b6e8e40b5f99f424bf9399c336e4eb7c','46fbf77c4e9831aa218b6611654c4c73','','','n',NULL,-1299257,100,-1299257,100,NULL),('21b81e306fd086d424910e412fdd3f5f','65d31498732bac9bfb03cb8cf4893d86','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-3000,100,-3000,100,NULL),('232e9b35d79835e7195f56c39b0bf89e','0d8b6ee871c09e25c1870ed203b99a4b','e51c1f47b657d70f26bf160e30b35354','','','n',NULL,-137100,100,-137100,100,NULL),('238d9b8cf286a8c14323a7274c8f1844','e63cdfe119cfdec2670f172232716884','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-3000,100,-3000,100,NULL),('258c8cb5f9617ffc1e1a914a747688d2','bdbc9e5820c8f9c1611da166a1e6d160','8fc91fbe3db81364bd56d3d4a4853807','','','n',NULL,-151700,100,-151700,100,NULL),('266e350e021eb83d308d034676392d4e','6971c041bd3d2583ffa1f07679e47306','c46156546b88ad5bb27f2d3dba01aebb','','','n',NULL,10000,100,10000,100,NULL),('26e93ad1552531ff7ce3017c1e974939','dbf57d84ba949e0823baf21cdd556215','2da96fc6d7160ed1faa3530e011ac6ae','','','n',NULL,6781600,100,6781600,100,NULL),('2777d1e76155d05e2bca68e789ce39ad','d572244f4e92bccd038ded7981fc2284','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,1000000,100,1000000,100,NULL),('29d6c235127105c08810b8cc24b84f3d','077078494de85727f7f630b0c9106802','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,-4300,100,-4300,100,NULL),('2ac041a395d1e5711c47001668f447b1','575c25f6de16c57579d0f8c1ea8167ce','73e0d82638c6ab8a656009e2ecaa8dad','','','n',NULL,100000,100,100000,100,NULL),('2c9596f43c4ca4db0858a3b7703abd1c','0761b5918a996dd4698938b34c269e05','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,59900,100,59900,100,NULL),('2cbd2fc1bbd15d1f10025a31cbe979a8','077078494de85727f7f630b0c9106802','422a30a247933efb47b17e9638722da7','','','n',NULL,4300,100,4300,100,NULL),('2dd3d59b3eb443e5eff79e1ffda86907','a74dc73a07af92341421ee55eb964242','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,-5000,100,-5000,100,NULL),('2e1ebbdfb6de3e53093e4c762ed47cad','c03b87e76255425bffa0aee7b7432ecd','f97a1cc92bbd40fd14d13b44490cc770','','','n',NULL,240000,100,599212,10000,NULL),('2e99667fb6288bc2abcae5e529bddcbb','d30dab89b7f07bba5726a8113575b1c3','127a7bccdea13690d45abef992b2779f','','Buy','n',NULL,196000,100,4,1,NULL),('3211a07c1ac15ef85186a90e1437a902','fae62e48fcb791386c9409aadab2d108','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,341531800,100,341531800,100,NULL),('3211eedfa3bb7cbb4fb572b38fda2970','933c00398c12be75d13db6ce8ab3f2a8','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-4000,100,-4000,100,NULL),('3322592ed412f12f5be7252068214d92','59f4bb98508a81d730009c9c9d9281f6','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-240000,100,-240000,100,NULL),('34847feb3a927615c693ef1c2dd28fb8','a822ced31614de312c17f87cffad2720','e51c1f47b657d70f26bf160e30b35354','','','n',NULL,-20195,100,-20195,100,NULL),('349c3bb577771f15ad42261b576f10b5','be17549d5b039e84a2b2cced4548abf9','3f3a7895f1bcd173de594539b1e12c48','','','n',NULL,702500,100,702500,100,NULL),('34c2ce8ae5b37787a76812a334cc9b27','6389ab8d436c597b76bc0b2bd7f4b03c','ee3a5de26dcbed105dbcbc629fd54612','','','n',NULL,-2500000,100,-2500000,100,NULL),('36e92097edf3cfe689a220203b0562b0','b8b687353c0e1d3fe7914eed36e432bc','455af3d10bb5a2c5b9c905afa64bfe5d','','','n',NULL,739000,100,739000,100,NULL),('39e74d2117bad995172c6aa090851504','afb57cee35f8c9f8bb1be6cab4fbc376','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,93946,100,93946,100,NULL),('3a0b17fdaf4e4e10498c6afc01629d14','2a18231dfd6da62c7642b64c0235755c','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,20000,100,20000,100,NULL),('3b663996355909b658f63bb905ef7d57','fc1553224a627c069c8a1bf2c178e039','ee3a5de26dcbed105dbcbc629fd54612','','','n',NULL,79980893,100,79980893,100,NULL),('3ba95d5810551c7a8e31e550849ba0c8','3e19e3e8ef011106186d7cd5dcc22819','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-20000,100,-20000,100,NULL),('3c913cc134d94a24089fe007ea13dee3','589c9881dceaaad9f4ab7130474aec00','e25012118b8a86970898b0fe2fcea20a','','','n',NULL,100,100,100,100,NULL),('3d7bf35cd728a35990114644adee36cb','33d6570a5732f1990927e51a627259aa','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-1000000,100,-1000000,100,NULL),('3e05e9b80f6b9ca61c56703a55a4e4f6','f666cb97b1e66890750571206a9203ce','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,40000,100,40000,100,NULL),('3ff57f42a6a8baa4414d6ce6634dad6d','7ea561b11ab354939e46aa10c63443b2','5986ada838a621cd06fae5c027fed6c7','','','n',NULL,61300,100,61300,100,NULL),('40042009e329c46626f81d609e42fc90','21c772bd72196c4770e2996d402c30f4','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,-1000000,100,-1000000,100,NULL),('41855885bd847e72619a24c0ca9b1a3d','0761b5918a996dd4698938b34c269e05','eba66f9e03bcfe6d853da967f6ec18ea','','','n',NULL,-59900,100,-59900,100,NULL),('43e7e477f95ae366615c8364c81ebb59','391e2eba383eb662996a033102cd5c89','925544064b77d21245b385e13c252d84','','Buy','n',NULL,154380,100,3,1,NULL),('481525d008527f56aa42fbd2b61cab13','6971c041bd3d2583ffa1f07679e47306','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-10000,100,-10000,100,NULL),('49905af123add85b0b70994b07d70dd2','d2e411ee24bedef4782853d8edc48f21','caf3feba0afc6f0b9f7bce678b56f392','','','n',NULL,20000,100,20000,100,NULL),('4a3e9cec3bc23198ec139e5350358e17','bae9df9759974fd8225ddcea9a3ecf46','8fc91fbe3db81364bd56d3d4a4853807','','','n',NULL,1693200,100,1693200,100,NULL),('4af5278a30b2e1bf806ccbaf8a8b25a1','86faf8409a4b93930063f924bb3437e4','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,500000,100,500000,100,NULL),('4c690c7e3180045f3b4687da9aaa4c30','3359352250a3097ec431860d34655d0f','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,-1000,100,-1000,100,NULL),('4ed4366e222b3bb118bf29b804f9600b','1d83712d01f2de580a519c6fb52010f8','f97a1cc92bbd40fd14d13b44490cc770','','Buy','n',NULL,2117457,100,5286690,10000,NULL),('4f2d60797a6e87fea70369a1f2969a3c','10083688ab53aa6c23e1fc9bf2c28761','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-150000,100,-150000,100,NULL),('4fe34721a72c6ae0b48e8d7849d518f7','3e19e3e8ef011106186d7cd5dcc22819','7216687f8a712f14af685716d2eead70','','','n',NULL,20000,100,20000,100,NULL),('5102bd6e2d965eaed379e1471c60470f','10cc11e31906bedf14896888d2520441','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-28000,100,-28000,100,NULL),('5113721a40e8387192a42d875cb7beee','84a926ab65fef1920a76aeb60106516b','422a30a247933efb47b17e9638722da7','','','n',NULL,3000,100,3000,100,NULL),('516e6fbcba6b3672948fa045b43e96b1','48a649b82343eac16a5a08efa1f84903','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,300000,100,300000,100,NULL),('5189f66c1629e391d0c51833dc3494dc','8f3fe30c92b8f02177cf3632e2ef34e3','caf3feba0afc6f0b9f7bce678b56f392','','','n',NULL,19500,100,19500,100,NULL),('52e0d5cb8ef89a18fb1eabce974eef3f','4af5764509cdcf82b33ec5febdbd2bf8','46fbf77c4e9831aa218b6611654c4c73','','','n',NULL,-500,100,-500,100,NULL),('5345370a45827a226e9a8c6c451a04dd','60bc46e0691530de731164d01e03a5c8','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-493196,100,-493196,100,NULL),('54d247e492d5c29219e4f594140dab73','f368661db658a8e87a7baf4774ef47ae','884670abc0e614e6c2ba581ec33926d9','','','n',NULL,10702,100,10702,100,NULL),('55b9e39f3c12324ac0cb071a132b27bb','42ff6ba8306a357c810c726e3324db2b','1edf8a7638e59599acbbe57ac24f7a8f','','Buy','n',NULL,240000,100,158134,10000,NULL),('5644455d3a8be59fe9c741ce91a34b6b','f2858a1f6ab1ee9d1a61c7e39f88e7d4','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-49016,100,-49016,100,NULL),('564d215bc613ed953fd262a01986b003','60bc46e0691530de731164d01e03a5c8','e7db8aaedf902655b44dc027ee03bb60','','Buy','n',NULL,493196,100,1173070,10000,NULL),('585129d601bc41b4e2c0c10c83fbe814','bdbc9e5820c8f9c1611da166a1e6d160','cb41053e15d88ee4c22d0320b84e9519','','','n',NULL,151700,100,151700,100,NULL),('587dfd4a019b360c708fd28f9a7eb2f4','fae62e48fcb791386c9409aadab2d108','9271ca1a07bf91fa11a6ec02d984939d','','','n',NULL,-341531800,100,-341531800,100,NULL),('58f46c258b7b091975b1bc4dfc5263c6','84a926ab65fef1920a76aeb60106516b','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-3000,100,-3000,100,NULL),('5a01b99806263f01536ee362bbf319c2','21c772bd72196c4770e2996d402c30f4','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,1000000,100,1000000,100,NULL),('5aa516bf990011e838ef7a1d302648eb','c9273d5810d3e5ba78d0126f66c678ea','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-6000,100,-6000,100,NULL),('5bfc729bdc27c1d7f7a3026ffac594b2','48a649b82343eac16a5a08efa1f84903','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,-300000,100,-300000,100,NULL),('61c6dcbb6b17f24145d0f48883e227c1','803a63c0f17cc5ba99bbc2ad7511388f','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-517531,100,-517531,100,NULL),('632a929bb9f8b53949130d6ec31b13dc','4c387a5f62a7338a0c40e4dc208f81db','c46156546b88ad5bb27f2d3dba01aebb','','','n',NULL,521000,100,521000,100,NULL),('638f801ae4c0267dfc50bc4b7411d1c9','575c25f6de16c57579d0f8c1ea8167ce','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-600000,100,-600000,100,NULL),('63e5a8c33a2a135934eda377d91bf26e','bf3d2238ea6885ab0a9e625d032b7d03','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,1000000,100,1000000,100,NULL),('641cd71293c11ac372e2472f4cfc3109','f72512e1ffe5b9c93418e1f8c4ad5065','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-1129845,100,-1129845,100,NULL),('650ceac8cf188100eea360c1030a897d','3f67008a0d820787b8602ae8d736cf55','e25012118b8a86970898b0fe2fcea20a','','','n',NULL,20700,100,20700,100,NULL),('6677e0ab76e39c7c792110a130db05b2','bf3d2238ea6885ab0a9e625d032b7d03','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,-1000000,100,-1000000,100,NULL),('68995694c2cf312950429512d4e6d0f1','d2e411ee24bedef4782853d8edc48f21','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,-20000,100,-20000,100,NULL),('6a175991aa4deab7033c1cd996dd984c','8a73255769dde9e94cafaf4e1ba7082a','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,-1500000,100,-1500000,100,NULL),('6d45c936b6412d06b8a1e6d2ce94c755','f357b8429d00dcc3c2bd8cca6b13fba6','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,500000,100,500000,100,NULL),('6e0a44bd242bb8ae824edf0a8eb61d33','e27f878d73618f45013d4b69deab9f33','6965f0a52e99ed48269f9b7f60797f42','','','n',NULL,100,100,100,100,NULL),('6e9dec2f5597e1e4e3b2ef2820f8404a','ea2daae2bd2dd1cc490524dc4dab749b','422a30a247933efb47b17e9638722da7','','','n',NULL,14000,100,14000,100,NULL),('6ff02c4aa4524522fe8136ee089f1ece','589c9881dceaaad9f4ab7130474aec00','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-100,100,-100,100,NULL),('71f8ce039a6d4ac4b75fc9f4e0e5793f','f368661db658a8e87a7baf4774ef47ae','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-10702,100,-10702,100,NULL),('72e4f351490ee9adce25cc1e4bf1292c','d2369d52fb1dfde26f27faea109a0ea7','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,702489,100,702489,100,NULL),('746c69c3f16759b2819d2036be0c3e51','8a73255769dde9e94cafaf4e1ba7082a','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,1500000,100,1500000,100,NULL),('75bdaa3548bb343d230b458721b43fdc','81f23103d5e5a98b00be07548e472a33','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-73230400,100,-73230400,100,NULL),('7616a96acda1858775eb46df5d03ab3f','cdf6b1f03b5040be52e1511d6ddcdedf','422a30a247933efb47b17e9638722da7','','','n',NULL,1000,100,1000,100,NULL),('77cb5b1616ce693e5e23c080b3cfcdb1','c03b87e76255425bffa0aee7b7432ecd','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-240000,100,-240000,100,NULL),('794cd33391e80311e5e7fffa7ce8dda0','fc1553224a627c069c8a1bf2c178e039','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-79980893,100,-79980893,100,NULL),('79d16395b7ec0b2bbbf2182e609debf3','c320c609d9985ca62fe1efae83662675','fe879ccb6fe227e284eb043f6a3998ef','','','n',NULL,366600,100,366600,100,NULL),('7a34d0ca1223735c8cdbdf3e1cdb1b33','abef812dac03dc03072da2f0829cba40','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-15000,100,-15000,100,NULL),('7a977a65f7231335b120f13c6d638f57','f72512e1ffe5b9c93418e1f8c4ad5065','27c4224f38bff085ef3bf0d61833e522','','','n',NULL,1129845,100,1129845,100,NULL),('7b5b4ea988d64d162e4c34ff22ba40fe','fd734f0d66ae4b5a7473a79e7a0c36fb','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-85317700,100,-85317700,100,NULL),('7cbf927cc6f3d1b013cc41c54ad6365a','3756375f95b9aacbc8166327da21b77c','8fc91fbe3db81364bd56d3d4a4853807','','','n',NULL,-1693044,100,-1693044,100,NULL),('7ea6176fe6f36cc130db232cf7f7f691','b6210aae4aacc9e210ab717d0adb4e2f','eba66f9e03bcfe6d853da967f6ec18ea','','','n',NULL,-21465600,100,-21465600,100,NULL),('824d9713310ff07caffd7f504683a65e','792989dd404b045b1649273db0ecc0ba','946bf0729b8cdf2db077b8b4df60fa3e','','Buy','n',NULL,138160,100,2,1,NULL),('826a18caaa63849c1f46c018ec178cc3','d569b17226d399954a8f6b0b59bbfe44','7f1aec77ac35b0e3c92a22cac0db86e5','','','n',NULL,15000,100,15000,100,NULL),('838cc6877eceb59ad251b0be0b3b141b','bab31494c317629328eccbe5c2b91b31','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-306000,100,-306000,100,NULL),('856e5588f6580f4bfc73dbe4c2a0aade','328d61f023ae899356dbc0a08c0355b2','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,200,100,200,100,NULL),('86dc416a19ba0d6f0e31e8f9166bfb75','1b6523e97820b8516d866dd7c0ea0920','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,2500000,100,2500000,100,NULL),('8716343da10fa0eb597276a3a1083917','165a027f497ced4cac8204a8750b0264','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,9,100,9,100,NULL),('873b04334f84635c80b23d571b5d775a','683502cd128985e6b85e20db695629f1','2fae6d0f0f15f42e208beedc9f2f6614','','','n',NULL,15000,100,15000,100,NULL),('89391dfdab983995793f3aa834578f0f','c63d571c8c8e731a43d215e6e37b85b1','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-2902200,100,-2902200,100,NULL),('89af22aa25757440bce564fa50d23b5e','f059491c0ad999312a4baa6e8421f48e','a9e9ffb50d365ee4990c529fc6e81220','','','n',NULL,71500,100,71500,100,NULL),('8a5ee99114d53f94589f2fb2cef3eb04','3359352250a3097ec431860d34655d0f','422a30a247933efb47b17e9638722da7','','','n',NULL,1000,100,1000,100,NULL),('8a8c459dc54ab28e6fb24a9a71e78847','4c387a5f62a7338a0c40e4dc208f81db','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-521000,100,-521000,100,NULL),('8bc09225ae4360523e55cfb7ee7cffe9','a63daec7afd9f4387618202ef3f8e98f','7253248269cfd24da0a6c3ac94cf4d8a','','Buy','n',NULL,2257333,100,3480000,10000,NULL),('8be447fad0787f614ea7ea57a47bbe00','d30dab89b7f07bba5726a8113575b1c3','e51c1f47b657d70f26bf160e30b35354','','','n',NULL,-196000,100,-196000,100,NULL),('8c5e395ebb706ccfd17e9ca65d272f76','62c5fd98887b1bbb11462bf2021a518f','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-2200291,100,-2200291,100,NULL),('8de70f3df688c000f3b6c716a538c490','48b6e9d480ef84f5896f7f89d1984542','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-88500,100,-88500,100,NULL),('8ec73fdc7ca8d7eaf2661ed55a848180','46088982285bbcb0ac415e1018c9d09d','ee3a5de26dcbed105dbcbc629fd54612','','','n',NULL,-1000000,100,-1000000,100,NULL),('90c1fb516df61b8dbf84a740434c49ed','3756375f95b9aacbc8166327da21b77c','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,1693044,100,1693044,100,NULL),('9161e22a13b4159411923350e6ec499f','c320c609d9985ca62fe1efae83662675','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-366600,100,-366600,100,NULL),('91738e3ace893d741e3305de68292835','bbc651b16602f69d6f3de3c4663cb142','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,-1000000,100,-1000000,100,NULL),('938959457bb1eeac15dddbdb71813f40','328d61f023ae899356dbc0a08c0355b2','eee1570cb845f0f957c7ee5302905367','','','n',NULL,-200,100,-200,100,NULL),('93b1102334cea226f4be1924b93ba046','d569b17226d399954a8f6b0b59bbfe44','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-15000,100,-15000,100,NULL),('93b8554d3f9880c67ffb0d2a2e40a5de','6736e94a8e4a90f613fedec0bd8b9fb5','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,-3000000,100,-3000000,100,NULL),('93f2982cc2efdfa49269dffd6028cb63','fd9aed71b43ae11435158c8b037870e5','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-1019300,100,-1019300,100,NULL),('94691d13546412361bc96936bb3f9152','ea2daae2bd2dd1cc490524dc4dab749b','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-14000,100,-14000,100,NULL),('98cbd597d51797d93806717dd3c9ea31','b8b687353c0e1d3fe7914eed36e432bc','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-739000,100,-739000,100,NULL),('99c15e2ab3215b56e3fdc4e564d3ef8d','1d83712d01f2de580a519c6fb52010f8','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-2117457,100,-2117457,100,NULL),('9c535665998b92febbc2b8739d50ef7f','6d00d5f5fdc8a086f716869413c67562','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,500000,100,500000,100,NULL),('9d2a2d3e6233b9cfffd0b3f14b2d015c','f8e204f029f89df492791072eacb4084','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,-11600000,100,-11600000,100,NULL),('9ec5d032302535cc64ac1c8141a768ee','575c25f6de16c57579d0f8c1ea8167ce','0731bc05804a4e132fbfa611a4ea1138','','','n',NULL,500000,100,500000,100,NULL),('9f36809b2556b53a381e09efabc17fff','3cb47cbc3c815e2def535b38d3ab66a0','caf3feba0afc6f0b9f7bce678b56f392','','','n',NULL,24000,100,24000,100,NULL),('a14c76820b8802807c8bda135bf57da9','4af5764509cdcf82b33ec5febdbd2bf8','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,500,100,500,100,NULL),('a266ad5684113bfba3bf03107c71aa20','10083688ab53aa6c23e1fc9bf2c28761','3fc86d569a9e7a27767ac24e7ec1aa18','','','n',NULL,150000,100,150000,100,NULL),('a492ec078b85958a1882bc3184202b50','08df1531f942dcbd48b18c27c177b541','1edf8a7638e59599acbbe57ac24f7a8f','','Buy','n',NULL,2200665,100,1450000,10000,NULL),('a51c6e19d28694f505e3baab5d3b0511','2518dea74982a7924a8b86483ac4e154','e51c1f47b657d70f26bf160e30b35354','','','n',NULL,-196410,100,-196410,100,NULL),('a7b71c79d1b15fd6886a31f501afe4f2','46088982285bbcb0ac415e1018c9d09d','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,1000000,100,1000000,100,NULL),('a84b79e448bf310875e1bbb9479e4635','48b6e9d480ef84f5896f7f89d1984542','a9e9ffb50d365ee4990c529fc6e81220','','','n',NULL,88500,100,88500,100,NULL),('a8f14f4dad1cd9e7cd0a14f1c093a4d0','fe962033a19efdbccee0c953f9898847','422a30a247933efb47b17e9638722da7','','','n',NULL,3000,100,3000,100,NULL),('a9192b6123e4fbb829cc94c2fb9aa27d','803a63c0f17cc5ba99bbc2ad7511388f','0e31f81088bbdcba65ea21fe1900aa25','','','n',NULL,22200,100,22200,100,NULL),('aa54bf37070c0af676439633e2592864','c6ea891f146f6b749947213e07a0a491','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-1500,100,-1500,100,NULL),('aba9a6d0c93b8dac896d6b2c0b935bf8','945e0c9ac670dc879c3e7381ce41f0cb','e51c1f47b657d70f26bf160e30b35354','','','n',NULL,-159840,100,-159840,100,NULL),('adab58f8bc179f7d7866b5db07542e65','1b6523e97820b8516d866dd7c0ea0920','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,-2500000,100,-2500000,100,NULL),('ae9b5fbb54988a71751f4695f39db914','f357b8429d00dcc3c2bd8cca6b13fba6','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-500000,100,-500000,100,NULL),('af9d678f3729b4708d042a8f3e26e8bc','dcdc54f084a3e7c9f652c7eedd36bc9a','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-400720,100,-400720,100,NULL),('b152c93689abc025bcaae157deb951ed','3de63b7a2bcbc128c53dc1b2a69cc00d','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,2560000,100,2560000,100,NULL),('b19198f2ac493ba23a0e71ec5bec511d','fd734f0d66ae4b5a7473a79e7a0c36fb','203ea0bbbf9cae3fc40bd8aa81e43caf','','','n',NULL,85317700,100,85317700,100,NULL),('b22c9889b3442eca042dfcf98da0fcde','e9409f498abb73587d6d06a26fec5431','ee3a5de26dcbed105dbcbc629fd54612','','','n',NULL,-1500000,100,-1500000,100,NULL),('b317b1acc29adba76e13fc8e1855080c','c17624c125501cf4634118e9931b278b','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,1000000,100,1000000,100,NULL),('b436ca9e94c12d7cbb6c6144fcb1192b','30f98796d9b773a4c19050430d1ac7c2','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-150000,100,-150000,100,NULL),('b92074c25e7e31b560cd209c454a7254','a822ced31614de312c17f87cffad2720','a9e9ffb50d365ee4990c529fc6e81220','','','n',NULL,20195,100,20195,100,NULL),('baabf7a0b0eb2f88987cd6bfdfe04420','91e3dec0025b93ab696ed3c37d2202c2','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-180000,100,-180000,100,NULL),('bba461cf142d7b7467a64faab238ea15','95a69367d8b657654229cdc3910905db','fe1e10f543b3b176e0ab0ba6e4455ea4','','','n',NULL,1843400,100,1843400,100,NULL),('bfb5acc44ffae154492a6650c29ccc02','f666cb97b1e66890750571206a9203ce','46fbf77c4e9831aa218b6611654c4c73','','','n',NULL,-40000,100,-40000,100,NULL),('c20de444bf63a377900dbea8276fd047','792989dd404b045b1649273db0ecc0ba','e51c1f47b657d70f26bf160e30b35354','','','n',NULL,-138160,100,-138160,100,NULL),('c2a78c0bc20c0316a5ec6dafffc3b066','a1fd051dc8f4554b92e351be4834ef84','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-80115,100,-80115,100,NULL),('c4c589b3439084859c3652d06e85e16e','5945071f283e32e4704c93f6e47945e4','0d21fec3e0fcb1619dc17f6fe139efc8','','','n',NULL,-11600000,100,-11600000,100,NULL),('c6b18fb87f0a081d9edde6e4a2a66485','95a69367d8b657654229cdc3910905db','ee3a5de26dcbed105dbcbc629fd54612','','','n',NULL,-1843400,100,-1843400,100,NULL),('c7cc0f5ff3905fcaa4af81def078a5c5','10cc11e31906bedf14896888d2520441','98b87123589a93713b98c61d5276baa5','','','n',NULL,28000,100,28000,100,NULL),('c84502792b82b1a5da847e46d2581368','e27f878d73618f45013d4b69deab9f33','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-100,100,-100,100,NULL),('c8e393e2d8f5544b7a423f865d346825','fd9aed71b43ae11435158c8b037870e5','e51c1f47b657d70f26bf160e30b35354','','','n',NULL,1019300,100,1019300,100,NULL),('c999a303031e967799e321f47d65917d','d572244f4e92bccd038ded7981fc2284','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-1000000,100,-1000000,100,NULL),('c9e272aa222b8f78a24df781c934d835','e32066d2382acb1c3fae620f5f0d4e60','422a30a247933efb47b17e9638722da7','','','n',NULL,3000,100,3000,100,NULL),('cc43d70468b11b62e7b81a3031f8438f','bab31494c317629328eccbe5c2b91b31','0d21fec3e0fcb1619dc17f6fe139efc8','','','n',NULL,306000,100,306000,100,NULL),('cf0bdc863ca6961f918bc20b4264c920','b6210aae4aacc9e210ab717d0adb4e2f','f36e7e8b06eb3e567186b1b7148208e5','','','n',NULL,20000,100,20000,100,NULL),('cf9d405871e8ef8e7019cd3f14055d09','abef812dac03dc03072da2f0829cba40','422a30a247933efb47b17e9638722da7','','','n',NULL,15000,100,15000,100,NULL),('d1f8d0b7db8c42fa9f0798dc8fe72f41','b8b7d27ab5a4b167b1f46cf1b899b0d9','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,2500,100,2500,100,NULL),('d3ced0d284fd30024e5dad29a53071e9','be17549d5b039e84a2b2cced4548abf9','5242f3b0472b95d88ffc7c548b11e3d6','','','n',NULL,-702500,100,-702500,100,NULL),('d413b5835fb325276314b975f7d1de3a','6389ab8d436c597b76bc0b2bd7f4b03c','ae8891bb416c6908b1e5c1e9c2c42660','','','n',NULL,2500000,100,2500000,100,NULL),('d6515f3771eeaaff4685c12323ffe5c4','6736e94a8e4a90f613fedec0bd8b9fb5','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,3000000,100,3000000,100,NULL),('d6719c61e451d4f90a91d265a1ee1a93','30f98796d9b773a4c19050430d1ac7c2','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,150000,100,150000,100,NULL),('d680bf8722225d4bd449f1e4053e0dbd','33d6570a5732f1990927e51a627259aa','7f1aec77ac35b0e3c92a22cac0db86e5','','','n',NULL,1000000,100,1000000,100,NULL),('d69a044351cbb62a759259baadae36b1','a74dc73a07af92341421ee55eb964242','422a30a247933efb47b17e9638722da7','','','n',NULL,5000,100,5000,100,NULL),('d6d62b3621933e83f6b26ea6f874a195','b6210aae4aacc9e210ab717d0adb4e2f','5242f3b0472b95d88ffc7c548b11e3d6','Description:BY TRANSFER INB Venkata Udai Kiran Sirimalla, Ref:2CTC730018600016 TRANSFER FROM 9856104187','','n',NULL,15519200,100,15519200,100,NULL),('dc5566ae1617257a68e8a46e398d0a47','42ff6ba8306a357c810c726e3324db2b','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-240000,100,-240000,100,NULL),('dd1b519c8dc999b003345f5fa4a946a0','2a18231dfd6da62c7642b64c0235755c','46fbf77c4e9831aa218b6611654c4c73','','','n',NULL,-20000,100,-20000,100,NULL),('dd2a2edba5ba34a3bda9d4068a188839','db47fca4525907d72f8eb3bc8135324e','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,848435,100,848435,100,NULL),('de15c69cbd2aea18a2ae647d6c9e91b6','7ea561b11ab354939e46aa10c63443b2','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-61300,100,-61300,100,NULL),('dedc53e432741c3e1dfaffea24ace52e','0d8b6ee871c09e25c1870ed203b99a4b','0b50a16343d5e07190dfa91d0fe91355','','Buy','n',NULL,137100,100,1,1,NULL),('e0084536da4bb175eeb1924d78a3322e','ab873d23fde9c394dff336e2e37eaaa5','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,2560000,100,2560000,100,NULL),('e16578c9b1e5b9a7bca49b322de10b40','e63cdfe119cfdec2670f172232716884','98b87123589a93713b98c61d5276baa5','','','n',NULL,3000,100,3000,100,NULL),('e1f98f351371666161c65be3b6fa2c66','3f67008a0d820787b8602ae8d736cf55','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-20700,100,-20700,100,NULL),('e2636b2c454ac9428d74cfaf41bf7543','81f23103d5e5a98b00be07548e472a33','446050a5d3b0efa8a78803a133808b8c','','','n',NULL,73230400,100,73230400,100,NULL),('e4124c211fea302522fb0903693d0695','bae9df9759974fd8225ddcea9a3ecf46','3d87a9a6fa3c92191e146c44115f0c07','','','n',NULL,-1693200,100,-1693200,100,NULL),('e534b746a9e46d7b5ad312fb35035e0f','f8e204f029f89df492791072eacb4084','0d21fec3e0fcb1619dc17f6fe139efc8','','','n',NULL,11600000,100,11600000,100,NULL),('e5a6348b9f6867241db3e6a63925ab78','6a4d6d5179e7baab0e120bf1749c63a1','27c4224f38bff085ef3bf0d61833e522','','','n',NULL,-100000,100,-100000,100,NULL),('e6fe9406e4a854e11141b9cee9348a11','b6e8e40b5f99f424bf9399c336e4eb7c','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,1299257,100,1299257,100,NULL),('e7448e24a2ed4850523f5fdb5917f533','945e0c9ac670dc879c3e7381ce41f0cb','58128fb41bfd11077b336535fdb2f407','','Buy','n',NULL,159840,100,4,1,NULL),('e8decac3c0bae122b46c345b06088208','803a63c0f17cc5ba99bbc2ad7511388f','3c237ec529e2c2c313ca60e2129dabcc','','','n',NULL,473131,100,473131,100,NULL),('e9c1f63302b96c1f21d5450c7dbd0c15','d2369d52fb1dfde26f27faea109a0ea7','3f3a7895f1bcd173de594539b1e12c48','','','n',NULL,-702489,100,-702489,100,NULL),('eaebbf7128d00216d7be6b1c6aacb6a3','18af0ba8c6b789f967ca19abd323c7ce','c36b3ebdd1e1701486319297cc13fe64','','','n',NULL,20000,100,20000,100,NULL),('ee03b85b0d03003091e32f6a59dbfe6d','cdf6b1f03b5040be52e1511d6ddcdedf','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-1000,100,-1000,100,NULL),('eeaf63f327c33b850e7e800c28c57a0a','391e2eba383eb662996a033102cd5c89','e51c1f47b657d70f26bf160e30b35354','','','n',NULL,-154380,100,-154380,100,NULL),('f0323dd7eb4bac56feeed60fe17ea9b0','b8b7d27ab5a4b167b1f46cf1b899b0d9','eee1570cb845f0f957c7ee5302905367','','','n',NULL,-2500,100,-2500,100,NULL),('f07896f0631513b9673ae16b6706d992','6a4d6d5179e7baab0e120bf1749c63a1','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,100000,100,100000,100,NULL),('f3e41938aa8418f7d8be30c58c81d0be','a63daec7afd9f4387618202ef3f8e98f','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-2257333,100,-2257333,100,NULL),('f42c8cfc705b452889628b57a89f66f8','165a027f497ced4cac8204a8750b0264','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-9,100,-9,100,NULL),('f4b48f72e1643bfd1187153b75a6cb9c','3cb47cbc3c815e2def535b38d3ab66a0','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-24000,100,-24000,100,NULL),('f72f590d305b8fdc906da80df8fafab6','f059491c0ad999312a4baa6e8421f48e','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,-71500,100,-71500,100,NULL),('fa9197c594415b4d1bc02f8f9afa1e2b','5945071f283e32e4704c93f6e47945e4','ee3a5de26dcbed105dbcbc629fd54612','','','n',NULL,11600000,100,11600000,100,NULL),('fb31be538c2276058d108c25a4a88d3d','f2858a1f6ab1ee9d1a61c7e39f88e7d4','98b87123589a93713b98c61d5276baa5','','','n',NULL,49016,100,49016,100,NULL),('fb61343e55ac3d69667279c8df3283d3','d904fe0c86e03a40bece802dda7c6885','4860fbd1bf2ac1587680e5b36bd7e436','','','n',NULL,92000,100,92000,100,NULL),('fc79ee2bd4e2fc8b3d4645518555ed64','62c5fd98887b1bbb11462bf2021a518f','6ddda8e5c5fe1717377da46c70b2d66a','','Buy','n',NULL,2200291,100,4183540,10000,NULL),('ff15718ec6310e08967dc35d9e9ef271','dbf57d84ba949e0823baf21cdd556215','649848eaf518be30a3f00ee5453bb59b','','','n',NULL,-6781600,100,-6781600,100,NULL),('ff2b41346824fb3a40961310fb6403ed','59f4bb98508a81d730009c9c9d9281f6','6ddda8e5c5fe1717377da46c70b2d66a','','','n',NULL,240000,100,452318,10000,NULL);
/*!40000 ALTER TABLE `splits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxtable_entries`
--

DROP TABLE IF EXISTS `taxtable_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxtable_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxtable` varchar(32) NOT NULL,
  `account` varchar(32) NOT NULL,
  `amount_num` bigint(20) NOT NULL,
  `amount_denom` bigint(20) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxtable_entries`
--

LOCK TABLES `taxtable_entries` WRITE;
/*!40000 ALTER TABLE `taxtable_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `taxtable_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxtables`
--

DROP TABLE IF EXISTS `taxtables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxtables` (
  `guid` varchar(32) NOT NULL,
  `name` varchar(50) NOT NULL,
  `refcount` bigint(20) NOT NULL,
  `invisible` int(11) NOT NULL,
  `parent` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxtables`
--

LOCK TABLES `taxtables` WRITE;
/*!40000 ALTER TABLE `taxtables` DISABLE KEYS */;
/*!40000 ALTER TABLE `taxtables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `guid` varchar(32) NOT NULL,
  `currency_guid` varchar(32) NOT NULL,
  `num` varchar(2048) NOT NULL,
  `post_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `enter_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `description` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`guid`),
  KEY `tx_post_date_index` (`post_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES ('0761b5918a996dd4698938b34c269e05','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-01 18:30:00','2018-05-02 04:42:20','BY TRANSFER INB Venkata Udai Kiran Sirimalla'),('077078494de85727f7f630b0c9106802','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-08 13:14:43','Snacks at office.'),('08df1531f942dcbd48b18c27c177b541','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-07 08:56:30',''),('0d8b6ee871c09e25c1870ed203b99a4b','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-08 18:30:00','2018-05-09 08:14:26','SIP May 2018'),('10083688ab53aa6c23e1fc9bf2c28761','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-03 18:30:00','2018-05-04 04:51:31','Apartment Maintenance fee 04/2018.'),('10cc11e31906bedf14896888d2520441','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-28 18:30:00','2018-04-30 13:06:03','POS 416021XXXXXX9633 More Superstore POS DEBIT'),('165a027f497ced4cac8204a8750b0264','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-29 18:30:00','2018-04-30 13:28:44','Opening Balances'),('18af0ba8c6b789f967ca19abd323c7ce','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 05:46:38','Paytm Recharge'),('1b6523e97820b8516d866dd7c0ea0920','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 06:35:08','Description:BY TRANSFER INB Payment towards loan repayment Ref:ITR2095619'),('1d83712d01f2de580a519c6fb52010f8','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-07 08:12:57','Opening Balance'),('21c772bd72196c4770e2996d402c30f4','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-01 05:44:18','TO TRANSFER UPI/812111338261/9949989024@upi'),('2518dea74982a7924a8b86483ac4e154','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-08 18:30:00','2018-05-09 08:13:21','SIP 2018'),('2a18231dfd6da62c7642b64c0235755c','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-04 18:30:00','2018-05-07 05:46:31','Paytm Recharge'),('30f98796d9b773a4c19050430d1ac7c2','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-02 04:40:50','NWD-416021XXXXXX9633-05916006-RAJENDRANAGAR'),('328d61f023ae899356dbc0a08c0355b2','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-01 18:30:00','2018-05-09 07:45:41','POS REF 416021******9633-05-01 PAYUIND'),('3359352250a3097ec431860d34655d0f','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 13:08:42','Snacks at office.'),('33d6570a5732f1990927e51a627259aa','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-02 04:39:21','NEFT Dr-ANDB0001295-IPHC-NETBANK, MUM-N121180530234055-Anusha-Udai Tithe'),('3756375f95b9aacbc8166327da21b77c','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-29 18:30:00','2018-04-30 10:29:32','Equity Opening Balance.'),('391e2eba383eb662996a033102cd5c89','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-08 18:30:00','2018-05-09 08:13:54','SIP May 2018'),('3cb47cbc3c815e2def535b38d3ab66a0','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-01 18:30:00','2018-05-02 04:49:42','Breakfast for home hyderguda attapur'),('3de63b7a2bcbc128c53dc1b2a69cc00d','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 06:35:40','Description:TO TRANSFER INB NEFT UTR NO: SBIN618127061616 SIRIMALLA VENKATA UDAI KIRAN ANUSHA Ref:NEFT INB: IRG4775266	'),('3e19e3e8ef011106186d7cd5dcc22819','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-01 18:30:00','2018-05-02 04:49:04','Petrol for Bike'),('3f67008a0d820787b8602ae8d736cf55','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-28 18:30:00','2018-04-30 13:09:33','POS 416021XXXXXX9633 PAYU-Ola Money - POS DEBIT	'),('42ff6ba8306a357c810c726e3324db2b','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 06:22:25','SI HDFC191767629 ICICI Prudenti-07-05-18'),('46088982285bbcb0ac415e1018c9d09d','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-09 18:30:00','2018-05-10 11:37:50','Description:TO TRANSFER  INB Payment towards loan repayment Ref:ITR2594311'),('48a649b82343eac16a5a08efa1f84903','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-09 18:30:00','2018-05-10 11:54:28','UPI Transfer for balance.'),('48b6e9d480ef84f5896f7f89d1984542','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-08 18:30:00','2018-05-09 07:41:04','POS 416021XXXXXX9633 NIMH NEURO SCIEN POS DEBIT'),('4af5764509cdcf82b33ec5febdbd2bf8','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-02 18:30:00','2018-05-03 05:28:31','Recharge paytm'),('4c387a5f62a7338a0c40e4dc208f81db','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-07 06:54:00','Clothes for Ammu'),('575c25f6de16c57579d0f8c1ea8167ce','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-02 04:39:50','NEFT Dr-CORP0000275-SIRIMALLA PAUL CORP-NETBANK, MUM-N121180530234603-expenses	'),('589c9881dceaaad9f4ab7130474aec00','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-28 18:30:00','2018-04-30 13:08:48','POS 416021XXXXXX9633 PAYU-www.olacabs POS DEBIT	'),('5945071f283e32e4704c93f6e47945e4','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-01 05:52:23','TO TRANSFER INB Expenses'),('59f4bb98508a81d730009c9c9d9281f6','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-09 18:30:00','2018-05-10 07:45:57','SI HDFC191871954 Mirae Asset Mu-10-05-18'),('60bc46e0691530de731164d01e03a5c8','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 08:05:58','Opening Balance'),('62c5fd98887b1bbb11462bf2021a518f','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-07 08:15:52','Opening Balance'),('6389ab8d436c597b76bc0b2bd7f4b03c','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 06:30:18','Description:TO TRANSFER INB Payment towards loan repayment Ref:ITR2095619'),('65d31498732bac9bfb03cb8cf4893d86','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-01 18:30:00','2018-05-02 07:55:54','Snacks in office.'),('6736e94a8e4a90f613fedec0bd8b9fb5','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-01 04:47:11','TO TRANSFER INB NEFT UTR NO: SBIN118121410186 SIRIMALLA VENKATA UDAI KIRAN ANUSHA'),('683502cd128985e6b85e20db695629f1','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-03 18:30:00','2018-05-04 04:49:58','Bike Maintenance'),('6971c041bd3d2583ffa1f07679e47306','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-05 18:30:00','2018-05-07 06:40:46','Expenses clothes.'),('6a4d6d5179e7baab0e120bf1749c63a1','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-01 18:30:00','2018-05-02 04:45:08','NFS/CASH WDL/01-05-18	'),('6d00d5f5fdc8a086f716869413c67562','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-08 05:52:40','UPI-00000020077230265-udai-sbi-vpa1@sbi-COLLECT-812811560096-Transferred'),('792989dd404b045b1649273db0ecc0ba','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-08 18:30:00','2018-05-09 08:15:24','SIP May 2018'),('7ea561b11ab354939e46aa10c63443b2','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-28 18:30:00','2018-04-30 13:06:24','POS 416021XXXXXX9633 VIDYA BOOK POINT POS DEBIT'),('803a63c0f17cc5ba99bbc2ad7511388f','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-05 18:30:00','2018-05-06 18:01:39','Description:POS 416021XXXXXX9633 SPENCERS POS DEBIT	Ref:000000011904'),('81f23103d5e5a98b00be07548e472a33','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-26 18:30:00','2018-04-27 10:11:57','Opening Balances.'),('84a926ab65fef1920a76aeb60106516b','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-02 18:30:00','2018-05-03 08:58:23','Snacks in office.'),('86faf8409a4b93930063f924bb3437e4','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-08 05:51:18','TO TRANSFER UPI/812811560096/s.udaikiran@okhdfcbank	'),('8a73255769dde9e94cafaf4e1ba7082a','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-08 05:42:17','Description:TO TRANSFER INB Payment towards loan repayment Ref:ITR2251226'),('8f3fe30c92b8f02177cf3632e2ef34e3','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 05:39:37','Breakfast for home'),('91e3dec0025b93ab696ed3c37d2202c2','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 09:53:18','TEMP MF EMND-355697726-674'),('933c00398c12be75d13db6ce8ab3f2a8','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-08 18:30:00','2018-05-10 05:07:33','Fruits for home'),('945e0c9ac670dc879c3e7381ce41f0cb','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-08 18:30:00','2018-05-09 08:14:50','SIP May 2018'),('95a69367d8b657654229cdc3910905db','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-29 18:30:00','2018-05-01 04:53:51','DEBIT INTEREST	'),('a1fd051dc8f4554b92e351be4834ef84','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-28 18:30:00','2018-04-30 13:07:05','POS 416021XXXXXX9633 282 PHD ATTAPUR POS DEBIT'),('a63daec7afd9f4387618202ef3f8e98f','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-07 08:20:22','Opening Balance'),('a74dc73a07af92341421ee55eb964242','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-03 18:30:00','2018-05-04 13:06:15','Snacks at office.'),('a822ced31614de312c17f87cffad2720','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-09 08:18:37','Borkerage Charges'),('ab873d23fde9c394dff336e2e37eaaa5','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 07:18:35','Description:NEFT Cr-SBIN0004155-Mr VENKATA UDAY KIRAN S-SIRIMALLA VENKATA UDAI KIRAN ANUSHA-SBIN618127061616 Ref:SBIN618127061616'),('abef812dac03dc03072da2f0829cba40','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-02 18:30:00','2018-05-04 04:52:06','Snacks for home'),('afb57cee35f8c9f8bb1be6cab4fbc376','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-26 18:30:00','2018-04-27 09:20:13','Opening Balance'),('b6210aae4aacc9e210ab717d0adb4e2f','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-01 04:32:39','Salary for 04/2108'),('b6e8e40b5f99f424bf9399c336e4eb7c','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-07 07:54:03','Opening Balance'),('b8b687353c0e1d3fe7914eed36e432bc','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-01 18:30:00','2018-05-02 04:48:02','Monthly Expenses for the month of May 2018'),('b8b7d27ab5a4b167b1f46cf1b899b0d9','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-09 07:41:54','UPI-917020028084740-goog-payment@okaxis-812811316568-UPI'),('bab31494c317629328eccbe5c2b91b31','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-26 18:30:00','2018-04-27 10:20:08','Opening Balance'),('bae9df9759974fd8225ddcea9a3ecf46','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 07:22:10','Description:NEFT Dr-CITI0000003-UDAI CITI CC-NETBANK, MUM-N127180535626792-CC Payment Ref:N127180535626792'),('bbc651b16602f69d6f3de3c4663cb142','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-08 05:52:31','UPI-00000020077230265-udai-sbi-vpa1@sbi-812811455089-transfer'),('bdbc9e5820c8f9c1611da166a1e6d160','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-09 13:00:00','2018-05-10 10:36:56','SouthPower Distbn Co Telangana Electricity payment for the month of April 2018.'),('be17549d5b039e84a2b2cced4548abf9','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-09 18:30:00','2018-05-10 11:42:45','Description:TO TRANSFER SBILT10052018171134619979'),('bf3d2238ea6885ab0a9e625d032b7d03','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-09 18:30:00','2018-05-10 11:39:01','Description:BY TRANSFER INB Payment towards loan repayment Ref:ITR2594311 '),('c03b87e76255425bffa0aee7b7432ecd','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 09:53:53','TEMP MF EMND-355698826-676	'),('c17624c125501cf4634118e9931b278b','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-08 05:50:59','TO TRANSFER UPI/812811455089/s.udaikiran@okhdfcbank'),('c320c609d9985ca62fe1efae83662675','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-28 18:30:00','2018-04-30 13:05:47','POS 416021XXXXXX9633 GHMCH POS DEBIT'),('c63d571c8c8e731a43d215e6e37b85b1','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-29 18:30:00','2018-04-30 13:22:51','Opening Balances'),('c6ea891f146f6b749947213e07a0a491','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-29 18:30:00','2018-04-30 12:37:12','Snacks'),('c9273d5810d3e5ba78d0126f66c678ea','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-09 18:30:00','2018-05-10 05:08:26','RTI application Fee and post at Hyderguda postoffice.'),('cdf6b1f03b5040be52e1511d6ddcdedf','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-03 18:30:00','2018-05-07 05:41:29','Snacks outside.'),('d2369d52fb1dfde26f27faea109a0ea7','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-07 07:55:42','Opening Balance'),('d2e411ee24bedef4782853d8edc48f21','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-02 18:30:00','2018-05-03 05:29:23','Breakfast for home'),('d30dab89b7f07bba5726a8113575b1c3','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-09 08:05:17','SIP May 2018'),('d569b17226d399954a8f6b0b59bbfe44','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-05 18:30:00','2018-05-07 06:41:30','Offerring at church.'),('d572244f4e92bccd038ded7981fc2284','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-02 04:40:42','NWD-416021XXXXXX9633-S1GA1591-RANGAREDDI'),('d904fe0c86e03a40bece802dda7c6885','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-29 18:30:00','2018-04-30 10:26:16','Opening Balance'),('db47fca4525907d72f8eb3bc8135324e','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-26 18:30:00','2018-04-27 10:18:55','Opening Balances'),('dbf57d84ba949e0823baf21cdd556215','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-26 18:30:00','2018-04-27 10:12:33','Opening Balance'),('dcdc54f084a3e7c9f652c7eedd36bc9a','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-07 08:09:12','Opening Balance'),('e27f878d73618f45013d4b69deab9f33','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-28 18:30:00','2018-04-30 13:04:43','POS 416021XXXXXX9633 UBER INDIA SYSTE POS DEBIT	'),('e32066d2382acb1c3fae620f5f0d4e60','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-08 18:30:00','2018-05-09 13:17:18','Snacks at office.'),('e63cdfe119cfdec2670f172232716884','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-08 18:30:00','2018-05-10 05:07:44','Groceries for home'),('e9409f498abb73587d6d06a26fec5431','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-08 05:40:56','Description:TO TRANSFER INB Payment towards loan repayment Ref:ITR2251226'),('ea2daae2bd2dd1cc490524dc4dab749b','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-08 18:30:00','2018-05-10 05:08:07','Snacks for home'),('f059491c0ad999312a4baa6e8421f48e','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-07 06:53:23','Misc.'),('f2858a1f6ab1ee9d1a61c7e39f88e7d4','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-26 18:30:00','2018-04-30 13:04:04','POS 416021XXXXXX9633 TRINETHRA SUPERR POS DEBIT'),('f357b8429d00dcc3c2bd8cca6b13fba6','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-07 18:30:00','2018-05-08 05:36:27','NWD-416021XXXXXX9633-05916075-HYDERABAD'),('f368661db658a8e87a7baf4774ef47ae','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-02 04:41:28','POS 416021XXXXXX9633 More Superstore POS DEBIT'),('f666cb97b1e66890750571206a9203ce','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-02 18:30:00','2018-05-03 05:28:25','Recharge paytm'),('f72512e1ffe5b9c93418e1f8c4ad5065','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-26 18:30:00','2018-04-27 10:17:44','Opening Balance'),('f8e204f029f89df492791072eacb4084','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-01 05:46:09','TO TRANSFER INB Expenses'),('fae62e48fcb791386c9409aadab2d108','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-26 18:30:00','2018-04-27 09:30:23','Opening Balance'),('fc1553224a627c069c8a1bf2c178e039','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-22 18:30:00','2018-05-02 17:44:59','Opening Balance'),('fd734f0d66ae4b5a7473a79e7a0c36fb','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-26 18:30:00','2018-04-27 10:11:34','Opening Balances.'),('fd9aed71b43ae11435158c8b037870e5','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-05-06 18:30:00','2018-05-09 07:43:58','Transfered to Zerodha.'),('fe962033a19efdbccee0c953f9898847','b417a83b048dbde8ef39c41ba0f0b0ce','','2018-04-30 18:30:00','2018-05-01 11:16:44','Curry for lunch');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendors` (
  `guid` varchar(32) NOT NULL,
  `name` varchar(2048) NOT NULL,
  `id` varchar(2048) NOT NULL,
  `notes` varchar(2048) NOT NULL,
  `currency` varchar(32) NOT NULL,
  `active` int(11) NOT NULL,
  `tax_override` int(11) NOT NULL,
  `addr_name` varchar(1024) DEFAULT NULL,
  `addr_addr1` varchar(1024) DEFAULT NULL,
  `addr_addr2` varchar(1024) DEFAULT NULL,
  `addr_addr3` varchar(1024) DEFAULT NULL,
  `addr_addr4` varchar(1024) DEFAULT NULL,
  `addr_phone` varchar(128) DEFAULT NULL,
  `addr_fax` varchar(128) DEFAULT NULL,
  `addr_email` varchar(256) DEFAULT NULL,
  `terms` varchar(32) DEFAULT NULL,
  `tax_inc` varchar(2048) DEFAULT NULL,
  `tax_table` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendors`
--

LOCK TABLES `vendors` WRITE;
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versions`
--

DROP TABLE IF EXISTS `versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `table_name` varchar(50) NOT NULL,
  `table_version` int(11) NOT NULL,
  PRIMARY KEY (`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versions`
--

LOCK TABLES `versions` WRITE;
/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` VALUES ('accounts',1),('billterms',2),('books',1),('budgets',1),('budget_amounts',1),('commodities',1),('customers',2),('employees',2),('entries',3),('Gnucash',2061200),('Gnucash-Resave',19920),('invoices',3),('jobs',1),('lots',2),('orders',1),('prices',2),('recurrences',2),('schedxactions',1),('slots',3),('splits',4),('taxtables',2),('taxtable_entries',3),('transactions',3),('vendors',1);
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-11 12:47:53
