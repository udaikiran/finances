import re
from datetime import datetime

class transaction:

    def __init__(self, tx_txt):
        if len(tx_txt) == 6:
            self.date = re.sub(r'\([^)]*\)', '',  ' '.join(tx_txt[0].split())).strip()
            self.description = ''.join(tx_txt[1].split())
            self.refno = ' '.join(tx_txt[2].split())
            self.debit = ' '.join(tx_txt[3].split())
            self.credit = ' '.join(tx_txt[4].split())
            self.balance = ' '.join(tx_txt[5].split())
        else:
            return None
        
        if len(self.debit) is 0:
            self.debit = '0.0'
        if len(self.credit) is 0:
            self.credit = '0.0'

        print("---------")
        print('date', self.date, datetime.strptime(self.date, '%d-%b-%Y'))
        print('description', self.description)
        print('refno', self.refno)
        print('debit', self.debit, float(self.debit.replace(',', '')))
        print('credit', self.credit, float(self.credit.replace(',', '')))
        print('balance', self.balance, float(self.balance.replace(',', '')))
        print("---------")
